package Tabs;

/**
 * Created by Nitin Sood on 30-03-2016.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skan_kontroll.BaseContainerFragment;
import com.skan_kontroll.R;

import Fragment.Approval_Fragment;

public class Approve_TabContainer extends BaseContainerFragment {

    private boolean IsViewInited;
    public static Approve_TabContainer approve_tabContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("Nitin", "Tab1");
        return inflater.inflate(R.layout.container_framelayout, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!IsViewInited) {
            IsViewInited = true;
            initView();
        }
    }

    private void initView() {
        replaceFragment(new Approval_Fragment(), false);
    }


}