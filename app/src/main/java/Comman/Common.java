package Comman;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by nitin_sood on 26-11-2015.
 */
public class Common {

    public static boolean Approval_API_Flag = true;
    public static boolean Leave_API_Flag = true;
    public static String SERVER_URL = "http://217.144.68.150/TimeBoxAPI/";
//    public static String SERVER_URL = "https://vju.skan-kontroll.no/TimeboxAPI/";
//    public static String SERVER_URL = "https://vju.skan-kontroll.no/TimeboxApiTest/";
//    public static String SERVER_URL = "https://vju.skan-kontroll.no/timeboxAPi/";
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static int Fragment_No = 0;

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static long getTimeUsedInMilliSeconds() {
        long timeMillisec = 0;
        Calendar calendar = Calendar.getInstance();
        long startTime = calendar.getTimeInMillis();
        return startTime;
    }

    public static String getDateDiffString(Date dateOne, Date dateTwo) {

        long timeOne = dateOne.getTime();
        long timeTwo = dateTwo.getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long delta = (timeTwo - timeOne) / oneDay;

        if (delta > 0) {
            return "" + delta;
        } else {
            delta *= -1;
            return "" + delta;
        }
    }


    public static String getMeNthParamInString(String p_text,
                                               String p_seperator, int nThParam) { // / "TOTRPIDS=101=104" returns
        // "101" If nThParam ==
        // 2.
        String retStrThirdParam = new String("**");
        int index = -1;
        int prevIdx = 0;
        int loopNM = 1;
        boolean loopBool = true;
        while (loopBool) {
            try {
                index = p_text.indexOf(p_seperator, prevIdx);
                if (loopNM >= nThParam) {
                    if (index >= 0) {
                        retStrThirdParam = p_text.substring(prevIdx, index);
                    } else // /-1
                    {
                        retStrThirdParam = p_text.substring(prevIdx);
                    }
                    loopBool = false;
                    break;
                } else {
                    if (index < 0) // /-1
                    {
                        loopBool = false;
                        retStrThirdParam = "**";
                        break;
                    }
                }
                loopNM++;
                prevIdx = index + 1;
            } catch (Exception ex) {
                loopBool = false;
                retStrThirdParam = "**";
                break;
            }
        } // /while
        if (retStrThirdParam.trim().length() <= 0) {
            retStrThirdParam = "**";
        }
        return retStrThirdParam;
    }

    public static double distance(double lat1, double lon1, double lat2,
                                  double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }

        return (dist);
    }


    public static String md5(String in) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update("ABC".getBytes());
            byte[] a = digest.digest();
            int len = a.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (int i = 0; i < len; i++) {
                sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
                sb.append(Character.forDigit(a[i] & 0x0f, 16));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Check string has value or not...
    public static boolean HasValue(Object input) {
        if (input == null || input.toString().length() == 0
                || "".equals(input.toString().trim())) {
            return false;
        }
        return true;
    }

    public static String get_count_of_days(String Created_date_String, String Expire_date_String) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        Date Created_convertedDate = null, Expire_CovertedDate = null, todayWithZeroTime = null;
        try {
            Created_convertedDate = dateFormat.parse(Created_date_String);
            Expire_CovertedDate = dateFormat.parse(Expire_date_String);


            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (Exception e) {
            e.printStackTrace();
        }


        int c_year = 0, c_month = 0, c_day = 0;

        if (Created_convertedDate.after(todayWithZeroTime)) {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(Created_convertedDate);

            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(todayWithZeroTime);

            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);
        }


            /*Calendar today_cal = Calendar.getInstance();
            int today_year = today_cal.get(Calendar.YEAR);
            int today = today_cal.get(Calendar.MONTH);
            int today_day = today_cal.get(Calendar.DAY_OF_MONTH);
            */


        Calendar e_cal = Calendar.getInstance();
        e_cal.setTime(Expire_CovertedDate);

        int e_year = e_cal.get(Calendar.YEAR);
        int e_month = e_cal.get(Calendar.MONTH);
        int e_day = e_cal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(c_year, c_month, c_day);
        date2.clear();
        date2.set(e_year, e_month, e_day);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);


        return ("" + (int) dayCount + " Days");
    }

    private static Dialog alertPopupWindow = null;

    public static void closeAlertPopupWindow() {
        if (null != alertPopupWindow) {
            alertPopupWindow.dismiss();
            alertPopupWindow = null;

        }
    }

    public static final int getRaduis(Context contex) {

        int density = contex.getResources().getDisplayMetrics().densityDpi;
        int den = 0;
        switch (density) {
            case DisplayMetrics.DENSITY_HIGH:
                den = 8;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                den = 5;
                break;
            case DisplayMetrics.DENSITY_LOW:
                den = 3;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                den = 18;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                den = 28;
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                den = 40;
                break;
            case DisplayMetrics.DENSITY_TV:
                den = 50;
                break;

            default:
                den = 20;
                break;
        }
        return den;
    }

    public static final int getDynamicTabsWeidth(Context contex, int count) {

        int density = contex.getResources().getDisplayMetrics().densityDpi;
        int den = 0;
        switch (density) {
            case DisplayMetrics.DENSITY_HIGH:
                if (count == 2) {
                    den = 275;
                } else if (count == 3) {
                    den = 200;
                } else if (count == 4) {
                    den = 210;
                }
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                if (count == 2) {
                    den = 180;
                } else if (count == 3) {
                    den = 90;
                } else if (count == 4) {
                    den = 50;
                }
                break;
            case DisplayMetrics.DENSITY_LOW:
                if (count == 2) {
                    den = 90;
                } else if (count == 3) {
                    den = 50;
                } else if (count == 4) {
                    den = 20;
                }
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                if (count == 2) {
                    den = 360;
                } else if (count == 3) {
                    den = 250;
                } else if (count == 4) {
                    den = 260;
                }
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                if (count == 2) {
                    den = 540;
                } else if (count == 3) {
                    den = 360;
                } else if (count == 4) {
                    den = 370;
                }
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                if (count == 2) {
                    den = 716;
                } else if (count == 3) {
                    den = 477;
                } else if (count == 4) {
                    den = 500;
                }

                break;
            case DisplayMetrics.DENSITY_TV:
                if (count == 2) {
                    den = 716;
                } else if (count == 3) {
                    den = 477;
                } else if (count == 4) {
                    den = 500;
                }
                break;

            default:
                if (count == 2) {
                    den = 716;
                } else if (count == 3) {
                    den = 477;
                } else if (count == 4) {
                    den = 500;
                }
                break;
        }
        return den;
    }

    public static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);

        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    public static final int getpopX(Context contex) {

        int density = contex.getResources().getDisplayMetrics().densityDpi;
        int den = 0;
        switch (density) {
            case DisplayMetrics.DENSITY_HIGH:
                den = 0;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                den = 0;
                break;
            case DisplayMetrics.DENSITY_LOW:
                den = 0;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                den = 0;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                den = 1000;
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                den = 0;
                break;
            case DisplayMetrics.DENSITY_TV:
                den = 0;
                break;

            default:
                den = 0;
                break;
        }
        return den;
    }

    public static final int getpopY(Context contex) {

        int density = contex.getResources().getDisplayMetrics().densityDpi;
        int den = 0;
        switch (density) {
            case DisplayMetrics.DENSITY_HIGH:
                den = 28;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                den = 18;
                break;
            case DisplayMetrics.DENSITY_LOW:
                den = 9;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                den = 45;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                den = 70;
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                den = 80;
                break;
            case DisplayMetrics.DENSITY_TV:
                den = 80;
                break;

            default:
                den = 80;
                break;
        }
        return den;
    }

    public static final int getpopHeight(Context contex) {

        int density = contex.getResources().getDisplayMetrics().densityDpi;
        int den = 0;
        switch (density) {
            case DisplayMetrics.DENSITY_HIGH:
                den = 200;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                den = 120;
                break;
            case DisplayMetrics.DENSITY_LOW:
                den = 40;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                den = 280;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                den = 430;
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                den = 550;
                break;
            case DisplayMetrics.DENSITY_TV:
                den = 550;
                break;

            default:
                den = 550;
                break;
        }
        return den;
    }

    public static Dialog showCustomAlertPopupWindow(final Context baseContext, final int popupLayoutResourceId) {

        alertPopupWindow = new Dialog(baseContext);
        alertPopupWindow.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertPopupWindow.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        //alertPopupWindow.getWindow().setWindowAnimations(R.anim.slideleft);
        alertPopupWindow.setContentView(popupLayoutResourceId);
        alertPopupWindow.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        alertPopupWindow.getWindow().setGravity(Gravity.CENTER_VERTICAL);
        // alertPopupWindow.setTitle("Custom Dialog");
        alertPopupWindow.setCancelable(true);
        alertPopupWindow.setCanceledOnTouchOutside(false);
        alertPopupWindow.show();

        // // cancel button to close pop-up window
        // final Button btnDismiss = (Button) alertPopupWindow
        // .findViewById(R.id.cancelButton);
        // if (btnDismiss != null) {
        // btnDismiss.setOnClickListener(new Button.OnClickListener() {
        // @Override
        // public void onClick(View v) {
        // closeAlertPopupWindow();
        // }
        // });
        // }

        // popupWindow.setFocusable(true);
        // popupWindow.update();
        return alertPopupWindow;

    }

    private static final double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /**
     * <p>
     * This function converts radians to decimal degrees.
     * </p>
     *
     * @param rad - the radian to convert
     * @return the radian converted to decimal degrees
     */
    private static final double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    // to show toast on screen
    public static void showToast(String msg, Context context) {
        try {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    public static boolean isAirplaneModeOn(Context context) {
        return Settings.System.getInt(context.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;
    }

    // Check SIM available or not
    public static boolean isSimAvailable(Context context) {

        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
            return true;
        } else {
            return false;
        }
    }

    public static final Typeface getfontStyle(Context context, Fontstyle ft) {
        Typeface typeface = null;

        if (ft == Fontstyle.FONT_LIGHT) {
            typeface = Typeface.createFromAsset(context.getAssets(),
                    "fonts/OpenSans-Light.ttf");
        } else if (ft == Fontstyle.FONT_REGULAR) {
            typeface = Typeface.createFromAsset(context.getAssets(),
                    "fonts/OpenSans-Regular.ttf");
        } else if (ft == Fontstyle.FONT_BOLDITALIC) {
            typeface = Typeface.createFromAsset(context.getAssets(),
                    "fonts/open-sans.semibold.ttf");
        }
        return typeface;
    }

    static public String urlEncodeForMsg(String sUrl) {// encoding url so that
        // we can hit it on
        // server
        StringBuffer urlOK = new StringBuffer();
        for (int i = 0; i < sUrl.length(); i++) {
            char ch = sUrl.charAt(i);
            switch (ch) {
                case '<':
                    urlOK.append("%3C");
                    break;
                case '>':
                    urlOK.append("%3E");
                    break;
                case ' ':
                    urlOK.append("%20");
                    break;
                case '-':
                    urlOK.append("%2D");
                    break;
                case '@':
                    urlOK.append("%40");
                    break;
                case '|':
                    urlOK.append("%7C");
                    break;
                default:
                    urlOK.append(ch);
                    break;
            }
        }
        return urlOK.toString().trim();
    }

    public enum Fontstyle {
        FONT_REGULAR, FONT_LIGHT, FONT_BOLDITALIC
    }

    public static final int getScreenDensity(Context contex) {

        int density = contex.getResources().getDisplayMetrics().densityDpi;
        int den = 0;
        switch (density) {
            case DisplayMetrics.DENSITY_HIGH:
                den = 170;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                den = 100;
                break;
            case DisplayMetrics.DENSITY_LOW:
                den = 50;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                den = 240;
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                den = 380;
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                den = 500;
                break;

            default:
                den = 500;
                break;
        }
        return den;
    }

    public static String down(String string) {

        try {
            java.net.URL url = new java.net.URL(string);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            String PATH = Environment.getExternalStorageDirectory().toString()
                    + "/GLP";

            File file = new File(PATH);
            File outputFile = new File(file, "GLP"
                    + Calendar.getInstance().getTimeInMillis() + ".apk");
            FileOutputStream fos = new FileOutputStream(outputFile);
            InputStream is = c.getInputStream();

            byte[] buffer = new byte[4096];
            int len1 = 0;

            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
            }

            fos.close();
            is.close();
            return outputFile.getAbsolutePath();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    private static final String ALPHA_NUMERIC_STRING = "ABCD1EF8GHI2JKLM3NPQ4RST5UVW6XYZ79";

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static String checkIfEndDateIsPastOrFuture(String endDate) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date compairdate = null;
        Date currentDate = new Date();
        String output = null;
        try {
            //Converting the input String to Date
            compairdate = df.parse(endDate);
            String currentDateTimeString = df.format(currentDate);
            try {
                currentDate = df.parse(currentDateTimeString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//            currentDate= df.parse(currentDate);
            if (compairdate.compareTo(currentDate) < 0) {
                result = "Past";
            } else if (compairdate.compareTo(currentDate) > 0) {
                result = "Future";
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static String checkIfTimePastOrFutureForLeave(String time, String currentTimeString) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("HH:mm");
        Date compairdate = null;
        Date currentDate = null;
        String output = null;
        try {
            //Converting the input String to Date
            compairdate = df.parse(time);

            currentDate = df.parse(currentTimeString);

//            currentDate= df.parse(currentDate);
            if (compairdate.compareTo(currentDate) < 0) {
                result = "Past";
            } else if (compairdate.compareTo(currentDate) > 0) {
                result = "Future";
            } else {
                result = "Current";
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static String checkIfTimePastOrFuture(String time, String currentTimeString) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("HH:mm");
        Date compairdate = null;
        Date currentDate = null;
        String output = null;
        try {
            //Converting the input String to Date
            compairdate = df.parse(time);
            if (currentTimeString.length() > 0) {
                String before = Common.getMeNthParamInString(currentTimeString, ".", 1);
                String after = Common.getMeNthParamInString(currentTimeString, ".", 2);
                currentTimeString = before + ":" + after;
            }
            currentDate = df.parse(currentTimeString);

//            currentDate= df.parse(currentDate);
            if (compairdate.compareTo(currentDate) < 0) {
                result = "Past";
            } else if (compairdate.compareTo(currentDate) > 0) {
                result = "Future";
            } else {
                result = "Current";
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static String checkDateForLeave(String endDate) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date compairdate = null;
        Date currentDate = new Date();
        String output = null;
        try {
            //Converting the input String to Date
            compairdate = df.parse(endDate);
            String currentDateTimeString = df.format(currentDate);
            try {
                currentDate = df.parse(currentDateTimeString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//            currentDate= df.parse(currentDate);
            if (compairdate.compareTo(currentDate) < 0) {
                result = "Past";
            } else if (compairdate.compareTo(currentDate) > 0) {
                result = "Future";
            } else {
                result = "Current";
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static String checkToDateLeave(String fromDate, String toDate) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date compairdate = null;
        Date currentDate = null;
        String output = null;
        try {
            //Converting the input String to Date
            compairdate = df.parse(fromDate);
            currentDate = df.parse(toDate);
//            currentDate= df.parse(currentDate);
            if (currentDate.compareTo(compairdate) < 0) {
                result = "Past";
            } else if (currentDate.compareTo(compairdate) > 0) {
                result = "Future";
            } else {
                result = "Current";
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static String checkIfEndDateIsPastOrFutureBeforeSaving(String endDate) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date compairdate = null;
        Date currentDate = null;
        String output = null;
        try {
            //Converting the input String to Date
            compairdate = df.parse(endDate);
            try {
                currentDate = df.parse("2016-03-27T08:45:00");
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//            currentDate= df.parse(currentDate);
            if (compairdate.compareTo(currentDate) < 0) {
                result = "Past";
            } else if (compairdate.compareTo(currentDate) > 0) {
                result = "Future";
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static String minusHours(String time) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date compairdate = null;
        String output = null;
        try {
            //Converting the input String to Date
            compairdate = df.parse(time);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(compairdate);
            calendar.add(Calendar.HOUR, +1);
            result = df.format(calendar.getTime());
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }
}
