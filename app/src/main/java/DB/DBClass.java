package DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import Comman.Common;
import Model.Approval_Model;
import Model.Leave_Model;

public class DBClass extends SQLiteOpenHelper {
    private static final String DB_NAME = "ScanControl.sqlite";
    private static String DB_PATH = "";
    private static DBClass mDBConnection;
    private SQLiteDatabase db;
    private final Context myContext;
    private static final int DATABASE_VERSION = 5;
    String reportDate1 = "";
    int mYearD = -1;
    int mMonthD = -1;
    int mDayD = -1;

    public DBClass(Context paramContext) {
        super(paramContext, "ScanControl.sqlite", null, DATABASE_VERSION);
        this.myContext = paramContext;
//		DB_PATH = "/data/data/"
//				+ paramContext.getApplicationContext().getPackageName()
//				+ "/databases/";
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            DB_PATH = myContext.getApplicationInfo().dataDir + "/databases/";
        } else {
            DB_PATH = "/data/data/" + myContext.getPackageName() + "/databases/";
        }
//        getReadableDatabase();
        try {
            checkOrCreateDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Calendar c = Calendar.getInstance();
        // System.out.println("Current time => " + c.getTime());
        //
        // SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
    }

    // To create database
    public void checkOrCreateDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
    }

    public void copyDataBaseOut() throws IOException {
        // Open your local db as the input stream
        InputStream myInput = new FileInputStream(DB_PATH + DB_NAME);
        // Path to the just created empty db
        File sd = Environment.getExternalStorageDirectory();

        // if the path doesn't exist first, create it
        File backupDB = new File(sd.getAbsolutePath() + "/ScanControlFilled.sqlite");
        if (!backupDB.exists()) {
            // backupDB.mkdir();
            backupDB.createNewFile();
        }

        // Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(backupDB);

        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public static boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READWRITE);

        } catch (SQLiteException e) {
            // database does't exist yet.
            e.printStackTrace();
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException {
        try {
            // Open your local db as the input stream
            InputStream myInput = myContext.getAssets().open(DB_NAME);

            // Path to the just created empty db
            String outFileName = DB_PATH + DB_NAME;

            // Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(outFileName);
            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static synchronized DBClass getDBAdapterInstance(Context context) {
        if (mDBConnection == null) {
            mDBConnection = new DBClass(context);
        }
        return mDBConnection;
    }

    @Override
    public void close() {
        if (this.db != null)
            this.db.close();
        super.close();

    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (dbExist) {
            // updateDB();
            // do nothing - database already exist
            Log.i("datbase exists", "db exists");
            // copyDataBaseOut();
//            this.getReadableDatabase();
//            try {
//                copyDataBase();
//            } catch (IOException e) {
//                throw new Error("Error copying database");
//            }
        } else {
            // By calling following method
            // 1) an empty database will be created into the default system path
            // of your application
            // 2) than we overwrite that database with our database.
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase paramSQLiteDatabase) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int oldVersion, int newVersion) {

    }


    public void openDataBase() throws SQLException {
        try {
            // copyDataBaseOut();
            String myPath = DB_PATH + DB_NAME;
            db = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READWRITE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // ////////////////////////////////////Application
    // Functions//////////////////////

    public void saveApprovalDataInDB(JSONArray approvalDataJsonArray) {
        try {
            String sql = "insert into customerarticledetail(Id,Article_Details,Comment,CustomerName," +
                    "EndDate,Has_Comment,Logged_Hours,Planned_Hours,StartDate,Task_bg_color,Task_text_color," +
                    "Modified_Date,ApprovalStatusId,IssueId,IssueWorkReportId,RequestId,WorkDate,IssueCommentId," +
                    "Address,City,Code,Street,TelephoneNumber1,TelephoneNumber2,VisitCityPostalCode,Flag,CustomerComment," +
                    "SchedulerComment) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            SQLiteStatement statement = this.db.compileStatement(sql);
            this.db.beginTransaction();
            if (approvalDataJsonArray.length() > 0) {
                for (int i = 0; i < approvalDataJsonArray.length(); i++) {

                    JSONObject json_data = approvalDataJsonArray
                            .getJSONObject(i);
                    String UserIdd = json_data.getString("UserId");
                    String StartDate = json_data.getString("StartDate");
                    String EndDate = json_data.getString("EndDate");
                    try {
                        if (StartDate.length() > 0) {
                            /////////// Checking if date is past of future and then show list accordingly
                            String listFlag = Common.checkIfEndDateIsPastOrFutureBeforeSaving(StartDate);
                            if (listFlag.equalsIgnoreCase("Future")) {
                                String beforeStartDate = Common.getMeNthParamInString(StartDate, "T", 1);
                                String afterStartDate = Common.getMeNthParamInString(StartDate, "T", 2);
                                afterStartDate = Common.minusHours(afterStartDate);
                                StartDate = beforeStartDate + "T" + afterStartDate;
                                if (EndDate.length() > 0) {
                                    String beforeEndDate = Common.getMeNthParamInString(EndDate, "T", 1);
                                    String afterEndDate = Common.getMeNthParamInString(EndDate, "T", 2);
                                    afterEndDate = Common.minusHours(afterEndDate);
                                    EndDate = beforeEndDate + "T" + afterEndDate;
                                }
                            }
                        }
                    } catch (Exception e) {
                        StartDate = json_data.getString("StartDate");
                        EndDate = json_data.getString("EndDate");
                    }

                    String CustomerName = json_data.getString("CustomerName");
                    String Article_Details = json_data.getString("Article_Details");
                    String Logged_Hours = json_data.getString("Logged_Hours");
                    String Planned_Hours = json_data.getString("Planned_Hours");
                    String Has_Comment = json_data.getString("Has_Comment");
                    String Comment = json_data.getString("Comment");
                    String Task_bg_color = json_data.getString("Task_bg_color");
                    String Task_text_color = json_data.getString("Task_text_color");
                    String IssueId = json_data.getString("IssueId");
                    String WorkDate = json_data.getString("WorkDate");
                    String IssueWorkReportId = json_data.getString("IssueWorkReportId");
                    String RequestId = json_data.getString("RequestId");
                    String ApprovalStatusId = json_data.getString("ApprovalStatusId");
                    String IssueCommentId = json_data.getString("IssueCommentId");
                    String Code = json_data.getString("Code");
                    String TelephoneNumber1 = json_data.getString("TelephoneNumber1");
                    String TelephoneNumber2 = json_data.getString("TelephoneNumber2");
                    String Address = json_data.getString("Address");
                    String Street = json_data.getString("Street");
                    String VisitCityPostalCode = json_data.getString("VisitCityPostalCode");
                    String City = json_data.getString("City");
                    String CustomerComment = json_data.getString("CustomerComment");
                    if (CustomerComment.equalsIgnoreCase("null") || CustomerComment == null) {
                        CustomerComment = "";
                    }
                    String Flag = json_data.getString("Flag");
                    String SchedulerComment = json_data.getString("SchedulerComment");
                    if (SchedulerComment.equalsIgnoreCase("null") || SchedulerComment == null) {
                        SchedulerComment = "";
                    }

                    statement.clearBindings();
                    statement.bindString(1, "" + i);
                    statement.bindString(2, Article_Details);
                    statement.bindString(3, Comment);
                    statement.bindString(4, CustomerName);
                    statement.bindString(5, EndDate);
                    statement.bindString(6, Has_Comment);
                    statement.bindString(7, Logged_Hours);
                    statement.bindString(8, Planned_Hours);
                    statement.bindString(9, StartDate);
                    statement.bindString(10, Task_bg_color);
                    statement.bindString(11, Task_text_color);
                    statement.bindString(12, "");
                    statement.bindString(13, ApprovalStatusId);
                    statement.bindString(14, IssueId);
                    statement.bindString(15, IssueWorkReportId);
                    statement.bindString(16, RequestId);
                    statement.bindString(17, WorkDate);
                    statement.bindString(18, IssueCommentId);
                    statement.bindString(19, Address);
                    statement.bindString(20, City);
                    statement.bindString(21, Code);
                    statement.bindString(22, Street);
                    statement.bindString(23, TelephoneNumber1);
                    statement.bindString(24, TelephoneNumber2);
                    statement.bindString(25, VisitCityPostalCode);
                    statement.bindString(26, Flag);
                    statement.bindString(27, CustomerComment);
                    statement.bindString(28, SchedulerComment);

                    statement.execute();
                }
            }
            this.db.setTransactionSuccessful();
            this.db.endTransaction();
//            copyDataBaseOut();
        } catch (Exception localException) {

            localException.printStackTrace();
            this.db.setTransactionSuccessful();
            this.db.endTransaction();
        }
    }

    public void deleteApprovalDataInDB() {
//        openDataBase();
        try {
            this.db.execSQL("delete from customerarticledetail");
        } catch (Exception localException) {

            localException.printStackTrace();
//            close();
        }
//        close();
    }

    public void saveLeavesDataInDB(JSONArray leavesJsonArray) {
        try {
            String sql = "insert into Leave(Id,LeaveStartDate,LeaveEndDate,LeaveType,WeekNumber,ApprovalStatusId,LeaveId) values(?,?,?,?,?,?,?);";
            SQLiteStatement statement = this.db.compileStatement(sql);
            this.db.beginTransaction();
            if (leavesJsonArray.length() > 0) {
                for (int i = 0; i < leavesJsonArray.length(); i++) {

                    JSONObject json_data = leavesJsonArray
                            .getJSONObject(i);
                    String Id = "" + i;
                    String LeaveStartDate = json_data.getString("LeaveStartDate");
                    String LeaveEndDate = json_data.getString("LeaveEndDate");
                    String LeaveType = json_data.getString("LeaveType");
                    String WeekNumber = json_data.getString("WeekNumber");
                    String ApprovalStatusId = json_data.getString("ApprovalStatusId");
                    String LeaveId = json_data.getString("LeaveId");


                    statement.clearBindings();
                    statement.bindString(1, Id);
                    statement.bindString(2, LeaveStartDate);
                    statement.bindString(3, LeaveEndDate);
                    statement.bindString(4, LeaveType);
                    statement.bindString(5, WeekNumber);
                    statement.bindString(6, ApprovalStatusId);
                    statement.bindString(7, LeaveId);

                    statement.execute();
                }
            }
            this.db.setTransactionSuccessful();
            this.db.endTransaction();
//            copyDataBaseOut();
        } catch (Exception localException) {

            localException.printStackTrace();
            this.db.setTransactionSuccessful();
            this.db.endTransaction();
        }
    }

    public void deleteLeaveDataInDB() {
//        openDataBase();
        try {
            this.db.execSQL("delete from Leave");
        } catch (Exception localException) {

            localException.printStackTrace();
//            close();
        }
//        close();
    }
    public void deleteLeaveFromDB(String delete_id) {
//        openDataBase();
        try {
            this.db.execSQL("delete from Leave where Id='"+delete_id+"'");
        } catch (Exception localException) {

            localException.printStackTrace();
//            close();
        }
//        close();
    }

    public ArrayList<Approval_Model> getDistinctLocationHome(String Query) {
//        openDataBase();

        ArrayList<Approval_Model> arr = new ArrayList<Approval_Model>();
        try {

            String query = Query;
            Cursor c = db.rawQuery(query, null);
            if (c.moveToFirst()) {
                do {
                    String UserIdd = c.getString(0);
                    String Article_Details = c.getString(1);
                    String Comment = c.getString(2);
                    String CustomerName = c.getString(3);
                    String EndDate = c.getString(4);
                    String Has_Comment = c.getString(5);
                    String Logged_Hours = c.getString(6);
                    String Planned_Hours = c.getString(7);
                    String StartDate = c.getString(8);
                    String Task_bg_color = c.getString(9);
                    String Task_text_color = c.getString(10);
                    String modifiefdate = c.getString(11);
                    String ApprovalStatusId = c.getString(12);
                    String IssueId = c.getString(13);
                    String IssueWorkReportId = c.getString(14);
                    String RequestId = c.getString(15);
                    String WorkDate = c.getString(16);
                    String IssueCommentId = c.getString(17);
                    String Address = c.getString(18);
                    String City = c.getString(19);
                    String Code = c.getString(20);
                    String Street = c.getString(21);
                    String TelephoneNumber1 = c.getString(22);
                    String TelephoneNumber2 = c.getString(23);
                    String VisitCityPostalCode = c.getString(24);
                    String Flag = c.getString(25);
                    String CustomerComment = c.getString(26);
                    String SchedulerComment = c.getString(27);

                    arr.add(new Approval_Model(UserIdd, StartDate, EndDate
                            , CustomerName, Article_Details, Logged_Hours
                            , Planned_Hours, Has_Comment, Comment
                            , Task_bg_color, Task_text_color, IssueId
                            , WorkDate, IssueWorkReportId, RequestId
                            , ApprovalStatusId, IssueCommentId, Code
                            , TelephoneNumber1, TelephoneNumber2, Address
                            , Street, VisitCityPostalCode, City
                            , CustomerComment, Flag, SchedulerComment));
                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        close();
        return arr;

    }

    public ArrayList<Leave_Model> getLeaveData(String Query) {
//        openDataBase();

        ArrayList<Leave_Model> arr = new ArrayList<Leave_Model>();
        try {

            String query = Query;
            Cursor c = db.rawQuery(query, null);
            if (c.moveToFirst()) {
                do {
                    String id = c.getString(0);
                    String LeaveStartDate = c.getString(1);
                    String LeaveEndDate = c.getString(2);
                    String LeaveType = c.getString(3);
                    String WeekNumber = c.getString(4);
                    String ApprovalStatusId = c.getString(5);
                    String LeaveId = c.getString(6);


                    arr.add(new Leave_Model(id, LeaveStartDate, LeaveEndDate
                            , LeaveType, WeekNumber, ApprovalStatusId
                            , LeaveId));
                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        close();
        return arr;

    }

    public void UpdatApprovalStatusID(String id, String StatusID) {
//        openDataBase();
        try {
            String Query = "UPDATE Customerarticledetail SET ApprovalStatusId ='" + StatusID.trim() + "' WHERE Id ='" + id.trim() + "'";
            this.db.execSQL(Query);
        } catch (Exception localException) {

            localException.printStackTrace();
//            close();
        }
//        close();
    }

    public void UpdatEndDateAfterSaved(String id, String EndDate, String Logged_Hours) {
//        openDataBase();
        try {
            String Query = "UPDATE Customerarticledetail SET EndDate='" + EndDate.trim() + "',Logged_Hours='" + Logged_Hours.trim() + "' WHERE Id ='" + id.trim() + "'";
            this.db.execSQL(Query);
        } catch (Exception localException) {

            localException.printStackTrace();
//            close();
        }
//        close();
    }

    public ArrayList<String> getAllDatesOFMonth(String Query) {
//        openDataBase();

        ArrayList<String> arr = new ArrayList<String>();
        try {

            String query = Query;
            Cursor c = db.rawQuery(query, null);
            if (c.moveToFirst()) {
                do {
                    String data = c.getString(0) + "~" + c.getString(1);
                    arr.add(data);
                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        close();
        return arr;

    }
}
