package DB;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class sharedPrefrances {

	public static void SaveFirstRun(Context context, int FirstRun) {
		try {
			SharedPreferences info = context.getSharedPreferences("FirstRun",
					Context.MODE_PRIVATE);
			Editor mEditor = info.edit();

			mEditor.clear();
			mEditor.commit();
			mEditor.putInt("FirstRun", FirstRun);
			mEditor.commit();
			mEditor = null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Integer getFirstRun(Context context) {
		SharedPreferences info = context.getSharedPreferences("FirstRun",
				Context.MODE_PRIVATE);

		int firstrun = info.getInt("FirstRun", 0);
		info = null;
		return firstrun;
	}


	public static void saveUserID(Context context, String UserID) {
		try {
			SharedPreferences info = context.getSharedPreferences("UserID",
					Context.MODE_PRIVATE);
			Editor savepass = info.edit();

			savepass.clear();
			savepass.commit();
			savepass.putString("UserID", UserID);
			savepass.commit();
			savepass = null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getUserID(Context context) {
		SharedPreferences info = context.getSharedPreferences("UserID",
				Context.MODE_PRIVATE);

		String savepass = info.getString("UserID", null);
		info = null;
		return savepass;
	}
    public static void saveUserName(Context context, String UserName) {
        try {
            SharedPreferences info = context.getSharedPreferences("UserName",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("UserName", UserName);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getUserName(Context context) {
        SharedPreferences info = context.getSharedPreferences("UserName",
                Context.MODE_PRIVATE);

        String savepass = info.getString("UserName", null);
        info = null;
        return savepass;
    }
    public static void saveUserEmail(Context context, String UserEmail) {
        try {
            SharedPreferences info = context.getSharedPreferences("UserEmail",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("UserEmail", UserEmail);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getUserEmail(Context context) {
        SharedPreferences info = context.getSharedPreferences("UserEmail",
                Context.MODE_PRIVATE);

        String savepass = info.getString("UserEmail", null);
        info = null;
        return savepass;
    }
    public static void saveUserFirstname(Context context, String UserFirstname) {
        try {
            SharedPreferences info = context.getSharedPreferences("UserFirstname",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("UserFirstname", UserFirstname);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getUserFirstname(Context context) {
        SharedPreferences info = context.getSharedPreferences("UserFirstname",
                Context.MODE_PRIVATE);

        String savepass = info.getString("UserFirstname", null);
        info = null;
        return savepass;
    }
    public static void saveUserLastName(Context context, String UserLastName) {
        try {
            SharedPreferences info = context.getSharedPreferences("UserLastName",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("UserLastName", UserLastName);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getUserLastName(Context context) {
        SharedPreferences info = context.getSharedPreferences("UserLastName",
                Context.MODE_PRIVATE);

        String savepass = info.getString("UserLastName", null);
        info = null;
        return savepass;
    }
    public static void saveUserDisplayName(Context context, String UserDisplayName) {
        try {
            SharedPreferences info = context.getSharedPreferences("UserDisplayName",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("UserDisplayName", UserDisplayName);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getUserDisplayName(Context context) {
        SharedPreferences info = context.getSharedPreferences("UserDisplayName",
                Context.MODE_PRIVATE);

        String savepass = info.getString("UserDisplayName", null);
        info = null;
        return savepass;
    }
    public static void saveEnteredUserName(Context context, String EnteredUserName) {
        try {
            SharedPreferences info = context.getSharedPreferences("EnteredUserName",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("EnteredUserName", EnteredUserName);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getEnteredUserName(Context context) {
        SharedPreferences info = context.getSharedPreferences("EnteredUserName",
                Context.MODE_PRIVATE);

        String savepass = info.getString("EnteredUserName", null);
        info = null;
        return savepass;
    }
    public static void saveEnteredPassword(Context context, String EnteredPassword) {
        try {
            SharedPreferences info = context.getSharedPreferences("EnteredPassword",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("EnteredPassword", EnteredPassword);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getEnteredPassword(Context context) {
        SharedPreferences info = context.getSharedPreferences("EnteredPassword",
                Context.MODE_PRIVATE);

        String savepass = info.getString("EnteredPassword", null);
        info = null;
        return savepass;
    }

    public static void saveMapPermissions(Context context, String MapPermissions) {
        try {
            SharedPreferences info = context.getSharedPreferences("MapPermissions",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("MapPermissions", MapPermissions);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getMapPermissions(Context context) {
        SharedPreferences info = context.getSharedPreferences("MapPermissions",
                Context.MODE_PRIVATE);

        String savepass = info.getString("MapPermissions", null);
        info = null;
        return savepass;
    }
    public static void saveActivityTimeStamp(Context context, String ActivityTimeStamp) {
        try {
            SharedPreferences info = context.getSharedPreferences("ActivityTimeStamp",
                    Context.MODE_PRIVATE);
            Editor savepass = info.edit();

            savepass.clear();
            savepass.commit();
            savepass.putString("ActivityTimeStamp", ActivityTimeStamp);
            savepass.commit();
            savepass = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getActivityTimeStamp(Context context) {
        SharedPreferences info = context.getSharedPreferences("ActivityTimeStamp",
                Context.MODE_PRIVATE);

        String savepass = info.getString("ActivityTimeStamp", null);
        info = null;
        return savepass;
    }
}
