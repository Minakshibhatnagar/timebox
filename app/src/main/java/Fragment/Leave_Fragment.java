package Fragment;

/**
 * Created by Nitin Sood on 30-03-2016.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.skan_kontroll.Home_Screen;
import com.skan_kontroll.R;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Calendar;

import Adapter.Leave_Adapter;
import Comman.Common;
import Comman.MyTextView_Regular;
import Comman.ProgressHUD;
import Comman.Webservices;
import DB.DBClass;
import DB.sharedPrefrances;
import Interface.leave_Interface;
import Model.Leave_Model;

public class Leave_Fragment extends Fragment implements View.OnClickListener, leave_Interface {

    private DBClass SkanDB;
    private SwipeRefreshLayout swipe_refresh_layout;
    public static SwipeMenuListView mListView;
    public static MyTextView_Regular mMyTextView_Regular_ErrorMsg;
    public static int menuFlag = 0;
    private int itemPositionForComment = 0;
    private ArrayList<Leave_Model> leave_list = new ArrayList<Leave_Model>();
    private Leave_Adapter leave_adapter;
    private ProgressHUD mProgressHUD;
    private String mLeavesResult = "";
    private String mDeleteLeavesResult = "";
    public static Leave_Fragment leave_fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.leaves_screen, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        try {
            // /////Initializing  DB
            SkanDB = DBClass.getDBAdapterInstance(getActivity());
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //// Initializing all static view in this function
        init();
        ///// Setting all views click Listener In this function
        setClicksListener();
        ///////// Showing leave data on screen
        showLeavesDataOnScreen();
        if (Common.Leave_API_Flag) {
            if (Webservices.isInternetOn(getActivity())) {
                /////////Getting Leave data from server
                Common.Leave_API_Flag = false;
                new GetLeavesDataFromServer().execute();
            }
        }
    }

    //// Initializing all static view in this function
    private void init() {
        leave_fragment = this;
        Common.Fragment_No = 3;
        Home_Screen.mLinearLayout_Plus.setVisibility(View.VISIBLE);
        Home_Screen.mLinearLayout_DateLayout.setVisibility(View.INVISIBLE);
        swipe_refresh_layout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipe_refresh_layout);
        mListView = (SwipeMenuListView) getActivity().findViewById(R.id.listView);
        mMyTextView_Regular_ErrorMsg = (MyTextView_Regular) getActivity().findViewById(R.id.MyTextView_ErrorMsg);
    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
// step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menuFlag) {
                    case 22:

                        break;
                    case 44:
                        // create "open" item
                        SwipeMenuItem Delete = new SwipeMenuItem(
                                getActivity().getApplicationContext());
                        // set item background
                        Delete.setBackground(R.drawable.round_delete_layout);
                        // set item width
                        Delete.setWidth(dp2px(110));
                        // set item title
                        Delete.setTitle(R.string.Delete);
                        // set item title fontsize
                        Delete.setTitleSize(16);
                        // set item title font color
                        Delete.setTitleColor(Color.WHITE);
                        // add to menu
                        menu.addMenuItem(Delete);
                        break;

                }

            }
        };
        // set creator
        mListView.setMenuCreator(creator);
        // step 2. listener item click event
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                String title = menu.getMenuItem(index).getTitle();
                itemPositionForComment = position;
                if (title.equalsIgnoreCase(getActivity().getResources().getString(R.string.Delete))) {
                    logoutDialog();
                }
                return false;
            }
        });
        // set SwipeListener
        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
//                Common.showToast("Start",getActivity());
                swipe_refresh_layout.setEnabled(false);
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
//                Common.showToast("End",getActivity());
                swipe_refresh_layout.setEnabled(true);
            }
        });


        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (mListView != null && mListView.getChildCount() > 0) {
                    // check if the first item of the list is visible
                    boolean firstItemVisible = mListView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = mListView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                swipe_refresh_layout.setEnabled(enable);
            }
        });
        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_layout.setRefreshing(true);
                if (Webservices.isInternetOn(getActivity())) {
                    /////////Getting Leave data from server
                    new GetLeavesDataFromServer().execute();
                } else {
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(R.string.Internet_error));
                }
            }
        });
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    private void showLeavesDataOnScreen() {
        leave_list.clear();
        leave_list = SkanDB.getLeaveData("select * from Leave order by WeekNumber");
        if (leave_list.size() > 0) {
            leave_adapter = new Leave_Adapter(getActivity(), leave_list);
            leave_adapter.notifyDataSetChanged();
            leave_adapter.notifyDataSetInvalidated();
            mListView.setAdapter(leave_adapter);
            mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);
            mMyTextView_Regular_ErrorMsg.setVisibility(View.GONE);
        } else {
            leave_list.clear();
            if (leave_adapter != null) {
                leave_adapter.notifyDataSetChanged();
            }
            mMyTextView_Regular_ErrorMsg.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void callDataRefersh() {
        if (Webservices.isInternetOn(getActivity())) {
            /////////Getting Leave data from server
            Common.Leave_API_Flag = false;
            new GetLeavesDataFromServer().execute();
        }
    }

    /////////Getting Leaves Data From Server
    private class GetLeavesDataFromServer extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            swipe_refresh_layout.setRefreshing(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                int year = Calendar.getInstance().get(Calendar.YEAR);
                String url = Common.SERVER_URL + "Api/Leave/GetUserLeaves";
                JSONObject LeaveJsonObject = new JSONObject();
                String UserName = sharedPrefrances.getUserName(getActivity());
                String EnteredUserName = sharedPrefrances.getEnteredUserName(getActivity());
                String EnteredPassword = sharedPrefrances.getEnteredPassword(getActivity());
                try {
                    LeaveJsonObject.put("OrganizationId", "12");
                    LeaveJsonObject.put("UserName", UserName);
                    LeaveJsonObject.put("LeaveStartDate", "01/01/" + year);
                    LeaveJsonObject.put("LeaveEndDate", "12/31/" + year);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mLeavesResult = Webservices.ApiCall(url, LeaveJsonObject,
                        getActivity(), 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mLeavesResult);
                if (mLeavesResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mLeavesResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        int resultcode = resultJsonObject.getInt("StatusCode");
                        if (resultcode == 200) {
                            //// Delete all previous records
                            SkanDB.deleteLeaveDataInDB();
                            JSONArray modelJsonArray = jsonObject
                                    .getJSONArray("Models");
                            if (modelJsonArray.length() > 0) {
                                /////Inserting bulk data to local DB
                                SkanDB.saveLeavesDataInDB(modelJsonArray);
                            }

                        } else {
//                            SnackbarManager.show(
//                                    Snackbar.with(getActivity())
//                                            .text(R.string.Leave_error_msg));
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mLeavesResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mLeavesResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mLeavesResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (swipe_refresh_layout.isRefreshing()) {
                swipe_refresh_layout.setRefreshing(false);
            }
            showLeavesDataOnScreen();
        }
    }

    ///////// Calling delete API on server
    private class DeleteLeaveAPI extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(getActivity(), true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Leave_Model leave_model = leave_list.get(itemPositionForComment);
                String LeaveStartDate = leave_model.getLeaveStartDate();
                String LeaveEndDate = leave_model.getLeaveEndDate();
                String delete_ID = leave_model.getId();
                String url = Common.SERVER_URL + "Api/Leave/DeleteUserLeaves";
                JSONObject LeaveJsonObject = new JSONObject();
                String UserName = sharedPrefrances.getUserName(getActivity());
                String EnteredUserName = sharedPrefrances.getEnteredUserName(getActivity());
                String EnteredPassword = sharedPrefrances.getEnteredPassword(getActivity());
                try {
                    LeaveJsonObject.put("LeaveStartDate", LeaveStartDate);
                    LeaveJsonObject.put("LeaveEndDate", LeaveEndDate);
                    LeaveJsonObject.put("UserName", UserName);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mDeleteLeavesResult = Webservices.ApiCall(url, LeaveJsonObject,
                        getActivity(), 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mDeleteLeavesResult);
                if (mDeleteLeavesResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mDeleteLeavesResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        int resultcode = resultJsonObject.getInt("StatusCode");
                        if (resultcode == 200) {
                            //// Delete leave from DB
                            SkanDB.deleteLeaveFromDB(delete_ID);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mDeleteLeavesResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mDeleteLeavesResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mDeleteLeavesResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            mProgressHUD.dismiss();
            showLeavesDataOnScreen();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }

    // dialog shown to confirm logout
    private void logoutDialog() {
        try {
            AlertDialog.Builder alert_box = new AlertDialog.Builder(getActivity());
            alert_box.setTitle(R.string.Dialog_Title);
            alert_box.setCancelable(false);
            alert_box.setMessage(R.string.delete_msg);
            alert_box.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    ////Checking if internet is available or not
                    if (Webservices.isInternetOn(getActivity())) {
                        //////// Deleting Leave on server
                        new DeleteLeaveAPI().execute();
                    } else {
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .text(R.string.Internet_error));
                    }
                    dialog.dismiss();
                }
            });
            alert_box.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = alert_box.create();
            dialog.show();
        } catch (Exception e) {
            // Tracking exception
        }
    }



    @Override
    public void onClick(View view) {

    }


}