package Fragment;

/**
 * Created by Nitin Sood on 30-03-2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.skan_kontroll.Approval_Options_Screen;
import com.skan_kontroll.BaseContainerFragment;
import com.skan_kontroll.Home_Screen;
import com.skan_kontroll.R;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Adapter.Approvel_Adapter;
import Comman.Common;
import Comman.MyTextView_Regular;
import Comman.ProgressHUD;
import Comman.Webservices;
import DB.DBClass;
import DB.sharedPrefrances;
import Interface.Change_Approval_list;
import Model.Approval_Model;

public class Approval_Fragment extends BaseContainerFragment implements Change_Approval_list {

    //    private List<ApplicationInfo> mAppList;
    private Approvel_Adapter mAdapter;
    public static SwipeMenuListView mListView;
    public static MyTextView_Regular mMyTextView_Regular_ErrorMsg;
    private ProgressHUD mProgressHUD;
    private String mApprovalResult = "";
    private String mCommentResult = "", mSubmitApprovalResult = "";
    private DBClass SkanDB;
    public static Approval_Fragment approval_fragment;
    private ArrayList<Approval_Model> approval_list = new ArrayList<Approval_Model>();
    private SwipeRefreshLayout swipe_refresh_layout;
    public static int menuFlag = 0;
    private int itemPositionForComment = 0;
    private String UserComment = "";
    private int responseResultcode = 0;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.approval, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        Log.e("Nitin", "Tab1 in");
        try {
            // /////Initializing  DB
            SkanDB = DBClass.getDBAdapterInstance(getActivity());
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        init();
        setClicksListener();
        //////Checking i we are hitting API once
        if (Common.Approval_API_Flag) {
            ////Checking if internet is available or not
            if (Webservices.isInternetOn(getActivity())) {
                /////////Getting Approval data from server
                Common.Approval_API_Flag = false;
                new callApprovalApi().execute();
            }
        }
        ///// Showing list according to date
        showApprovalListOnScreen(Home_Screen.formattedDateForDB);
    }

    //// Initializing all static view in this function
    private void init() {
        Common.Fragment_No=0;
        Home_Screen.mLinearLayout_DateLayout.setVisibility(View.VISIBLE);
        Home_Screen.mLinearLayout_Plus.setVisibility(View.INVISIBLE);
        approval_fragment = this;
//        mAppList = getActivity().getPackageManager().getInstalledApplications(0);
        swipe_refresh_layout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipe_refresh_layout);
        mListView = (SwipeMenuListView) getActivity().findViewById(R.id.listView);
        mMyTextView_Regular_ErrorMsg = (MyTextView_Regular) getActivity().findViewById(R.id.MyTextView_ErrorMsg);

    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
// step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menuFlag) {
                    case 22:

                        break;
                    case 44:
                        // create "open" item
                        SwipeMenuItem Approve = new SwipeMenuItem(
                                getActivity().getApplicationContext());
                        // set item background
                        Approve.setBackground(R.drawable.round_approve_layout);
                        // set item width
                        Approve.setWidth(dp2px(110));
                        // set item title
                        Approve.setTitle(R.string.Approve);
                        // set item title fontsize
                        Approve.setTitleSize(16);
                        // set item title font color
                        Approve.setTitleColor(Color.WHITE);
                        // add to menu
                        menu.addMenuItem(Approve);

                        SwipeMenuItem comment = new SwipeMenuItem(
                                getActivity().getApplicationContext());
                        // set item background
                        comment.setBackground(R.drawable.round_comment_layout);
                        // set item width
                        comment.setWidth(dp2px(110));
                        // set item title
                        comment.setTitle(R.string.comment);
                        // set item title fontsize
                        comment.setTitleSize(16);
                        // set item title font color
                        comment.setTitleColor(Color.WHITE);
                        // add to menu
                        menu.addMenuItem(comment);
                        break;
                    case 55:
                        SwipeMenuItem comment2 = new SwipeMenuItem(
                                getActivity().getApplicationContext());
                        // set item background
                        comment2.setBackground(R.drawable.round_comment_layout);
                        // set item width
                        comment2.setWidth(dp2px(110));
                        // set item title
                        comment2.setTitle(R.string.comment);
                        // set item title fontsize
                        comment2.setTitleSize(16);
                        // set item title font color
                        comment2.setTitleColor(Color.WHITE);
                        // add to menu
                        menu.addMenuItem(comment2);
                        break;
                }

            }
        };
        // set creator
        mListView.setMenuCreator(creator);
        // step 2. listener item click event
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                String title = menu.getMenuItem(index).getTitle();
                itemPositionForComment = position;
                if (title.equalsIgnoreCase(getActivity().getResources().getString(R.string.comment))) {
                    CommentPOPUP(position);
                } else if (title.equalsIgnoreCase(getActivity().getResources().getString(R.string.Approve))) {
                    ////Checking if internet is available or not
                    if (Webservices.isInternetOn(getActivity())) {
                        /////////Submitting Approval data from server
                        new SubmitApproveStatusOnServer().execute();
                    } else {
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .text(R.string.Internet_error));
                    }
                }
                return false;
            }
        });
        // test item long click
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), Approval_Options_Screen.class);
                i.putExtra("ModelClass", approval_list.get(position));
                startActivity(i);
            }
        });
        // set SwipeListener
        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
//                Common.showToast("Start",getActivity());
                swipe_refresh_layout.setEnabled(false);
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
//                Common.showToast("End",getActivity());
                swipe_refresh_layout.setEnabled(true);
            }
        });


        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (mListView != null && mListView.getChildCount() > 0) {
                    // check if the first item of the list is visible
                    boolean firstItemVisible = mListView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = mListView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                swipe_refresh_layout.setEnabled(enable);
            }
        });
        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_layout.setRefreshing(true);
                if (Webservices.isInternetOn(getActivity())) {
//            /////////Getting Approval data from server
                    new callApprovalApi().execute();
                }
            }
        });
    }

    ///// Showing list according to date
    private void showApprovalListOnScreen(String Date) {
        approval_list.clear();
        approval_list = SkanDB.getDistinctLocationHome("select * from Customerarticledetail where strftime('%Y-%m-%d', StartDate) ='" + Date.trim() + "'");
        if (approval_list.size() > 0) {
            ArrayList<Object> approval_listSort = new ArrayList<Object>();
            for (int i = 0; i < approval_list.size(); i++) {
                String EndDate = approval_list.get(i).getEndDate();
                String ApprovalStatusId = approval_list.get(i).getApprovalStatusId();
                Object model_object = approval_list.get(i);
                /////////// Checking if date is past of future and then show list accordingly
                String listFlag = Common.checkIfEndDateIsPastOrFuture(EndDate);
                if (listFlag.equalsIgnoreCase("Future")) {

                } else {
                    if (!ApprovalStatusId.equalsIgnoreCase("1") && !ApprovalStatusId.equalsIgnoreCase("4")) {
                        approval_listSort.add(model_object);
                    }
                }

            }
            if (approval_listSort.size() > 0) {
                for (int i = 0; i < approval_listSort.size(); i++) {
                    Object model_object = approval_listSort.get(i);
                    approval_list.remove(model_object);
                }
            }

            if (approval_list.size() > 0) {
                mAdapter = new Approvel_Adapter(getActivity(), approval_list);
                mAdapter.notifyDataSetChanged();
                mAdapter.notifyDataSetInvalidated();
                mListView.setAdapter(mAdapter);
                mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);
                mMyTextView_Regular_ErrorMsg.setVisibility(View.GONE);
            } else {
                approval_list.clear();
                if(mAdapter!=null){
                    mAdapter.notifyDataSetChanged();
                }
                mMyTextView_Regular_ErrorMsg.setVisibility(View.VISIBLE);
            }

        } else {
            approval_list.clear();
            if(mAdapter!=null){
                mAdapter.notifyDataSetChanged();
            }
            mMyTextView_Regular_ErrorMsg.setVisibility(View.VISIBLE);
        }
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public void change_Approval_List(String date) {
        ///// Showing list according to date
        showApprovalListOnScreen(date);
    }

    /////////Getting Approval data from server
    private class callApprovalApi extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
//            mProgressHUD = ProgressHUD.show(getActivity(), true,
//                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String url = Common.SERVER_URL + "Api/CustomerArticleDetail/GetAllArticleDetails";
                JSONObject ApprovalJsonObject = new JSONObject();
                String UserId = sharedPrefrances.getUserID(getActivity());
                String UserName = sharedPrefrances.getUserName(getActivity());
                String EnteredUserName = sharedPrefrances.getEnteredUserName(getActivity());
                String EnteredPassword = sharedPrefrances.getEnteredPassword(getActivity());
                try {
                    ApprovalJsonObject.put("UserId", UserId);
                    ApprovalJsonObject.put("UserName", UserName);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mApprovalResult = Webservices.ApiCall(url, ApprovalJsonObject,
                        getActivity(), 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mApprovalResult);
                if (mApprovalResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mApprovalResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        int resultcode = resultJsonObject.getInt("StatusCode");
                        if (resultcode == 200) {
                            //// Delete all previous records
                            SkanDB.deleteApprovalDataInDB();
                            JSONArray modelJsonArray = jsonObject
                                    .getJSONArray("Models");
                            if (modelJsonArray.length() > 0) {
                                /////Inserting bulk data to local DB
                                SkanDB.saveApprovalDataInDB(modelJsonArray);
                            }

                        } else {

                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mApprovalResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mApprovalResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mApprovalResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (swipe_refresh_layout.isRefreshing()) {
                swipe_refresh_layout.setRefreshing(false);
            }
            showApprovalListOnScreen(Home_Screen.formattedDateForDB);
//            mProgressHUD.dismiss();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            if (swipe_refresh_layout.isRefreshing()) {
                swipe_refresh_layout.setRefreshing(false);
            }
//            mProgressHUD.dismiss();
        }
    }

    private void CommentPOPUP(final int position) {
        try {
            final Dialog CommentPopupView = new Dialog(getActivity());
            CommentPopupView.requestWindowFeature(Window.FEATURE_NO_TITLE);
            CommentPopupView.getWindow().setBackgroundDrawableResource(
                    android.R.color.transparent);
            //alertPopupWindow.getWindow().setWindowAnimations(R.anim.slideleft);
            CommentPopupView.setContentView(R.layout.comment_popup);
            CommentPopupView.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            CommentPopupView.getWindow().setGravity(Gravity.CENTER_VERTICAL);
            // alertPopupWindow.setTitle("Custom Dialog");
            CommentPopupView.setCancelable(true);
            CommentPopupView.setCanceledOnTouchOutside(false);

            ////////////////
            LinearLayout LinearLayout_Toplay = (LinearLayout) CommentPopupView
                    .findViewById(R.id.LinearLayout_Toplay);
            RoundRectShape rect = new RoundRectShape(
                    new float[]{30, 30, 30, 30, 30, 30, 30, 30},
                    null,
                    null);
            int raduis=Common.getRaduis(getActivity());
            LinearLayout_Toplay.setBackgroundResource(R.drawable.round_top_layout);
            GradientDrawable gd = (GradientDrawable) LinearLayout_Toplay.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#53aed1"));
            gd.setCornerRadii(new float[]{raduis, raduis, raduis, raduis, 0, 0, 0, 0});
            gd.setStroke(1, Color.parseColor("#53aed1"), 5, 6);
            ///////////////////////////
            final EditText EditText_comment = (EditText) CommentPopupView
                    .findViewById(R.id.EditText_comment);

            final TextView TextView_Cancel = (TextView) CommentPopupView
                    .findViewById(R.id.TextView_Cancel);
            TextView_Cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(TextView_Cancel.getWindowToken(), 0);
                    CommentPopupView.dismiss();
                }
            });
            final TextView TextView_Done = (TextView) CommentPopupView
                    .findViewById(R.id.TextView_Done);
            TextView_Done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (EditText_comment.getText().toString().length() == 0) {
                        Common.showToast(getActivity().getResources().getString(R.string.Blank_comment), getActivity());
                    } else {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(TextView_Done.getWindowToken(), 0);
                        itemPositionForComment = position;
                        UserComment = EditText_comment.getText().toString().trim();
                        CommentPopupView.dismiss();
                        if (Webservices.isInternetOn(getActivity())) {
                            ////////submit Comment On Server
                            new SubmitCommentOnServer().execute();
                        } else {
                            Common.showToast(getActivity().getResources().getString(R.string.Internet_error), getActivity());
                        }
                    }
                }
            });
            CommentPopupView.show();
        } catch (Exception e) {
            // Tracking exception
        }
    }

    /////////submit Comment On Server
    private class SubmitCommentOnServer extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(getActivity(), true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String url = Common.SERVER_URL + "Api/WorkReportItem/CommentTask";
                JSONObject CommentJsonObject = new JSONObject();
                Approval_Model approval_model = approval_list.get(itemPositionForComment);
                String UserId = sharedPrefrances.getUserID(getActivity());
                /////////// Getting current date and time
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date currentDate = new Date();
                ///////////////// Getting Week number of year
                Calendar calender = Calendar.getInstance();
                int WeekNumber = calender.get(Calendar.WEEK_OF_YEAR);
                //// Converting date to string format
                String RequestDate = df.format(currentDate);
                ///////////////// Getting saved User name and password
                String EnteredUserName = sharedPrefrances.getEnteredUserName(getActivity());
                String EnteredPassword = sharedPrefrances.getEnteredPassword(getActivity());
                ///////// Converting string values in Integer values
                int IssueId = Integer.parseInt(approval_model.getIssueId());
                int RequestId = Integer.parseInt(approval_model.getRequestId());
                ////////// Adding data in json object
                try {
                    CommentJsonObject.put("UserId", UserId);
                    CommentJsonObject.put("RequestDate", RequestDate);
                    CommentJsonObject.put("ApprovalStatusId", approval_model.getApprovalStatusId());//integer
                    CommentJsonObject.put("WeekNumber", WeekNumber);//current week of the year integer
                    CommentJsonObject.put("IssueId", IssueId);//Integer value
                    CommentJsonObject.put("WorkDate", approval_model.getWorkDate());
                    CommentJsonObject.put("RequestId", RequestId);//Integer value
                    CommentJsonObject.put("Comment", UserComment);
                    CommentJsonObject.put("SaveComment", 1);//Integer
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mCommentResult = Webservices.ApiCall(url, CommentJsonObject,
                        getActivity(), 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mCommentResult);
                if (mCommentResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mCommentResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        int resultcode = resultJsonObject.getInt("StatusCode");
                        if (resultcode == 200) {

                        } else {
                            Common.showToast(getActivity().getResources().getString(R.string.Comment_server_error), getActivity());
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mCommentResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mCommentResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mCommentResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            mProgressHUD.dismiss();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }

    /////////Submit Approve Status On Server
    private class SubmitApproveStatusOnServer extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(getActivity(), true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String url = Common.SERVER_URL + "Api/WorkReportItem/ApproveTask";
                JSONObject ApproveStatusJsonObject = new JSONObject();
                Approval_Model approval_model = approval_list.get(itemPositionForComment);
                String UserId = sharedPrefrances.getUserID(getActivity());
                /////////// Getting current date and time
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date currentDate = new Date();
                //// Converting date to string format
                String RequestDate = df.format(currentDate);
                ///////////////// Getting Week number of year
                String WorkDate = approval_model.getWorkDate();
                Date workdt = df.parse(WorkDate);
                ///////////////// Getting Week number of year
//                Calendar calender = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(workdt);
                int WeekNumber = calendar.get(Calendar.WEEK_OF_YEAR);
                ///////////////// Getting saved User name and password
                String EnteredUserName = sharedPrefrances.getEnteredUserName(getActivity());
                String EnteredPassword = sharedPrefrances.getEnteredPassword(getActivity());
                ///////// Converting string values in Integer values
                int IssueId = Integer.parseInt(approval_model.getIssueId());
                int RequestId = Integer.parseInt(approval_model.getRequestId());
                ////////// Adding data in json object
                try {
                    ApproveStatusJsonObject.put("UserId", UserId);
                    ApproveStatusJsonObject.put("RequestDate", RequestDate);
                    ApproveStatusJsonObject.put("ApprovalStatusId", "2");
                    ApproveStatusJsonObject.put("WeekNumber", WeekNumber);//current week of the year integer data
                    ApproveStatusJsonObject.put("IssueId", IssueId);//integer
                    ApproveStatusJsonObject.put("WorkDate", approval_model.getWorkDate());
                    ApproveStatusJsonObject.put("RequestId", RequestId);//integer
                    ApproveStatusJsonObject.put("Comment", "");
                    ApproveStatusJsonObject.put("SaveComment", 0);//integer // on approve screen 0 an in edit screen 1
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mSubmitApprovalResult = Webservices.ApiCall(url, ApproveStatusJsonObject,
                        getActivity(), 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mCommentResult);
                if (mSubmitApprovalResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mSubmitApprovalResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        responseResultcode = resultJsonObject.getInt("StatusCode");

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mSubmitApprovalResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mSubmitApprovalResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mSubmitApprovalResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (responseResultcode == 200) {
                approval_list.get(itemPositionForComment).setApprovalStatusId("2");
                String id = approval_list.get(itemPositionForComment).getUserIdd();
                SkanDB.UpdatApprovalStatusID(id, "2");
                showApprovalListOnScreen(Home_Screen.formattedDateForDB);
            } else {
                SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .text(R.string.Task_Server_error));
            }
            mProgressHUD.dismiss();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }
}