package com.skan_kontroll;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.content.Context;

import DB.DBClass;
import DB.sharedPrefrances;

public class Splash_Screen extends Activity {
    private static int SPLASH_TIME_OUT = 2000;
    private DBClass SkanDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash__screen);
        // /////Creating DB
        SkanDB = DBClass.getDBAdapterInstance(Splash_Screen.this);
        try {
            ///Creating DB
            SkanDB.createDataBase();
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {

			/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                int FirstRun = sharedPrefrances.getFirstRun(Splash_Screen.this);
                if (FirstRun == 0) {
                    if (isMyServiceRunning(Logout_Service.class)) {
                        stopService(new Intent(Splash_Screen.this, Logout_Service.class));
                    }
                    Intent i = new Intent(Splash_Screen.this, Login_Screen.class);
                    startActivity(i);
                } else {
                    if (isMyServiceRunning(Logout_Service.class)==false) {
                        startService(new Intent(Splash_Screen.this, Logout_Service.class));
                    }
                    Intent i = new Intent(Splash_Screen.this, Home_Screen.class);
                    startActivity(i);
                }
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
