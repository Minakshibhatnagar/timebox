package com.skan_kontroll;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import Comman.MyTextView_Regular;
import DB.sharedPrefrances;


public class Cust_Task_info_Screen extends Activity implements View.OnClickListener {

    private LinearLayout mLinearLayout_Back;
    private MyTextView_Regular mMyTextView_Regular_Address, mMyTextView_Regular_TaskInfo;
    private String mCustomerComment="",mAddress="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cust__task_info__screen);
        //// Initializing all static view in this function
        init();
        ///// Setting all views click Listener In this function
        setClicksListener();
        //// change top status bar color same as Action bar color
        changeStatusBarColor();
    }
    //// change top status bar color same as Action bar color
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = Cust_Task_info_Screen.this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(Cust_Task_info_Screen.this.getResources().getColor(R.color.Status_Bar_Color));
        }
    }
    //// Initializing all static view in this function
    private void init() {
        ///////////////
        // 1. get passed intent
        Intent intent = getIntent();
        // 2. get CustomerComment  from intent which is sent from previous activity
        mCustomerComment = intent.getStringExtra("CustomerComment");
        mAddress = intent.getStringExtra("Address");
        /////////////
        mLinearLayout_Back = (LinearLayout) findViewById(R.id.LinearLayout_Back);
        mMyTextView_Regular_Address = (MyTextView_Regular) findViewById(R.id.MyTextView_Address);
        mMyTextView_Regular_TaskInfo = (MyTextView_Regular) findViewById(R.id.MyTextView_TaskInfo);
        /////////////
        mMyTextView_Regular_Address.setText(mAddress);
        mMyTextView_Regular_TaskInfo.setText(mCustomerComment);
    }
    ///// Setting all views click Listener In this function
    private void setClicksListener() {
        mLinearLayout_Back.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
         if (view == mLinearLayout_Back) {
            finish();
        }
    }
    @Override
    public void onUserInteraction() {
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentDate = new Date();
        String currentDateTimeString = df.format(currentDate);

        sharedPrefrances.saveActivityTimeStamp(Cust_Task_info_Screen.this, currentDateTimeString);
        Log.e("Touching",
                "Touching done......");
    }
}
