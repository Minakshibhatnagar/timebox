package com.skan_kontroll;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TimePicker;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import junit.framework.Test;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import Comman.Common;
import Comman.MyTextView_Regular;
import Comman.ProgressHUD;
import Comman.RangeTimePickerDialog;
import Comman.Webservices;
import DB.DBClass;
import DB.sharedPrefrances;
import Fragment.Approval_Fragment;
import Fragment.CalendarFragment;
import Model.Approval_Model;


public class Approval_ChangeHrs_Screen extends Activity implements View.OnClickListener {
    private DBClass SkanDB;
    private ImageView mImageView_Map;
    private MyTextView_Regular mMyTextView_Regular_cust_name, mMyTextView_Regular_Article_Details, mMyTextView_Regular_PlannedHrs, mMyTextView_Regular_LoggedHrs;
    private MyTextView_Regular mMyTextView_StartTime, mMyTextView_EndTime;
    private LinearLayout mLinearLayout_PlannedHRS, mLinearLayout_LoggedHRS;
    private EditText mEditText_comment;
    private Button mButton_Cancel, mButton_Save, mButton_Approve;
    private ScrollView mScrollView_Main;
    private Approval_Model mApproval_model;
    static final int TIME_DIALOG_ID_Logged = 1111;
    static final int TIME_DIALOG_ID_Start = 2222;
    static final int TIME_DIALOG_ID_End = 3333;
    private int loggedhour;
    private int loggedminute;
    private int starthour;
    private int startminute;
    private int endhour;
    private int endminute;
    private ProgressHUD mProgressHUD;
    private String mSaveCommentResult = "";
    private String LoggedHRS = "";
    private int responseResultcode = 0;
    private String outputEndTime = "";
    private String savedEndDate = "";
    private String mSubmitApprovalResult = "";
    private String topStartTime = "";
    private String topEndTime = "";
    private boolean ApproveFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approval__change_hrs__screen);
        try {
            // /////Initializing  DB
            SkanDB = DBClass.getDBAdapterInstance(this);
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //// Initializing all static view in this function
        init();
        ///// Setting all views click Listener In this function
        setClicksListener();
        //// change top status bar color same as Action bar color
        changeStatusBarColor();
    }
    //// change top status bar color same as Action bar color
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = Approval_ChangeHrs_Screen.this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(Approval_ChangeHrs_Screen.this.getResources().getColor(R.color.Status_Bar_Color));
        }
    }
    //// Initializing all static view in this function
    private void init() {
        /////////////
        // 1. get passed intent
        Intent intent = getIntent();
        // 2. get Approval_Model object from intent which is sent from previous activity
        mApproval_model = (Approval_Model) intent.getSerializableExtra("ModelClass");
        ////////////////////
        mImageView_Map = (ImageView) findViewById(R.id.ImageView_Map);
        ///////////////////////
        mMyTextView_Regular_cust_name = (MyTextView_Regular) findViewById(R.id.MyTextView_TaskName);
        mMyTextView_Regular_Article_Details = (MyTextView_Regular) findViewById(R.id.MyTextView_Address);
        mMyTextView_StartTime = (MyTextView_Regular) findViewById(R.id.MyTextView_StartTime);
        mMyTextView_EndTime = (MyTextView_Regular) findViewById(R.id.MyTextView_EndTime);
        mMyTextView_Regular_PlannedHrs = (MyTextView_Regular) findViewById(R.id.MyTextView_PlannedHrs);
        mMyTextView_Regular_PlannedHrs.setTypeface(Common.getfontStyle(Approval_ChangeHrs_Screen.this, Common.Fontstyle.FONT_BOLDITALIC));
        mMyTextView_Regular_LoggedHrs = (MyTextView_Regular) findViewById(R.id.MyTextView_LoggedHrs);
        mMyTextView_Regular_LoggedHrs.setTypeface(Common.getfontStyle(Approval_ChangeHrs_Screen.this, Common.Fontstyle.FONT_BOLDITALIC));
        ///////////////////
        mLinearLayout_PlannedHRS = (LinearLayout) findViewById(R.id.LinearLayout_PlannedHRS);
        mLinearLayout_LoggedHRS = (LinearLayout) findViewById(R.id.LinearLayout_LoggedHRS);
        ///////////////
        mEditText_comment = (EditText) findViewById(R.id.EditText_comment);
        /////////////////
        mButton_Cancel = (Button) findViewById(R.id.Button_Cancel);
        mButton_Save = (Button) findViewById(R.id.Button_Save);
        mButton_Approve = (Button) findViewById(R.id.Button_Approve);
        /////////////////
        mScrollView_Main = (ScrollView) findViewById(R.id.ScrollView_Main);
        //////////////// Getting data from model class
        String cust_name = mApproval_model.getCustomerName();
        String Article_Details = mApproval_model.getArticle_Details();
        String PlannedHrs = mApproval_model.getPlanned_Hours();
        LoggedHRS = mApproval_model.getLogged_Hours();
        if (LoggedHRS.length() > 0) {
            String before = Common.getMeNthParamInString(LoggedHRS, ".", 1);
            String after = Common.getMeNthParamInString(LoggedHRS, ".", 2);
            if (after.length() == 1) {
                after = after + "0";
                LoggedHRS = before + "." + after;
            }
        }
        String address = mApproval_model.getAddress();
        String startdate = mApproval_model.getStartDate();
        String EndDate = mApproval_model.getEndDate();
        topStartTime = convertDate(startdate);
        topEndTime = convertDate(EndDate);


        /////////////// setting data in Text Fields
        mMyTextView_Regular_cust_name.setText(cust_name);
        mMyTextView_Regular_Article_Details.setText(Article_Details);
        mMyTextView_Regular_PlannedHrs.setText(PlannedHrs);
        mMyTextView_Regular_LoggedHrs.setText(LoggedHRS);
        mMyTextView_StartTime.setText(topStartTime);
        mMyTextView_EndTime.setText(topEndTime);
        ///////////////////////////
    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
        mButton_Cancel.setOnClickListener(this);
        mButton_Save.setOnClickListener(this);
        mButton_Approve.setOnClickListener(this);
        mLinearLayout_PlannedHRS.setOnClickListener(this);
        mLinearLayout_LoggedHRS.setOnClickListener(this);
        mMyTextView_StartTime.setOnClickListener(this);
        mMyTextView_EndTime.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mButton_Cancel) {
            finish();
        } else if (view == mButton_Save) {
//            if (!LoggedHRS.equalsIgnoreCase(mMyTextView_Regular_LoggedHrs.getText().toString().trim())) {
                if (mEditText_comment.getText().toString().length() != 0) {
                    ////Checking if internet is available or not
                    if (Webservices.isInternetOn(Approval_ChangeHrs_Screen.this)) {
                        /////////Save Logged Hours On Server
                        ApproveFlag = false;
                        new SaveLoggedHoursOnServer().execute();
                    } else {
                        SnackbarManager.show(
                                Snackbar.with(Approval_ChangeHrs_Screen.this)
                                        .text(R.string.Internet_error));
                    }
                } else {
                    SnackbarManager.show(
                            Snackbar.with(Approval_ChangeHrs_Screen.this)
                                    .text(R.string.Blank_Comment_error));
                }

//            } else {
//                SnackbarManager.show(
//                        Snackbar.with(Approval_ChangeHrs_Screen.this)
//                                .text(R.string.Comment_error));
//            }

        } else if (view == mButton_Approve) {
            if (mEditText_comment.getText().toString().length() != 0) {
                ////Checking if internet is available or not
                if (Webservices.isInternetOn(Approval_ChangeHrs_Screen.this)) {
                    /////////Save Logged Hours On Server
                    ApproveFlag = true;
                    new SaveLoggedHoursOnServer().execute();

                } else {
                    SnackbarManager.show(
                            Snackbar.with(Approval_ChangeHrs_Screen.this)
                                    .text(R.string.Internet_error));
                }
            } else {
                SnackbarManager.show(
                        Snackbar.with(Approval_ChangeHrs_Screen.this)
                                .text(R.string.Blank_Comment_error));
            }
        } else if (view == mLinearLayout_LoggedHRS) {
//            String Logged_hrs = mMyTextView_Regular_LoggedHrs.getText().toString().trim();
//            if (Logged_hrs.length() > 0 && Logged_hrs != null && !Logged_hrs.equalsIgnoreCase("null")) {
//                loggedhour = Integer.parseInt(Common.getMeNthParamInString(Logged_hrs, ".", 1));
//                loggedminute = Integer.parseInt(Common.getMeNthParamInString(Logged_hrs, ".", 2));
//            }
//            new TimePickerDialog(Approval_ChangeHrs_Screen.this, timePickerListenerLogged, loggedhour, loggedminute,
//                    true).show();
        } else if (view == mMyTextView_StartTime) {
            String StartTime = mMyTextView_StartTime.getText().toString().trim();
            if (StartTime.length() > 0 && StartTime != null && !StartTime.equalsIgnoreCase("null")) {
                starthour = Integer.parseInt(Common.getMeNthParamInString(StartTime, ":", 1));
                startminute = Integer.parseInt(Common.getMeNthParamInString(StartTime, ":", 2));
            }
            new TimePickerDialog(Approval_ChangeHrs_Screen.this, timePickerListenerStart, starthour, startminute,
                    true).show();
        } else if (view == mMyTextView_EndTime) {
            String EndTime = mMyTextView_EndTime.getText().toString().trim();
            if (EndTime.length() > 0 && EndTime != null && !EndTime.equalsIgnoreCase("null")) {
                endhour = Integer.parseInt(Common.getMeNthParamInString(EndTime, ":", 1));
                endminute = Integer.parseInt(Common.getMeNthParamInString(EndTime, ":", 2));
            }
            new TimePickerDialog(Approval_ChangeHrs_Screen.this, timePickerListenerEnd, endhour, endminute,
                    true).show();
        }
    }

    private String convertDate(String datestr) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat("HH:mm");
        Date date = null;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(datestr);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);
            result = output;
            //Displaying the date
            System.out.println(output);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    //    @Override
//    public Dialog onCreateDialog(int id) {
//        switch (id) {
//            case TIME_DIALOG_ID_Logged:
//
//                // set time picker as current time
//                RangeTimePickerDialog boundTimePickerDialogLogged = new RangeTimePickerDialog(this, timePickerListenerLogged, loggedhour, loggedminute,
//                        true);
//                boundTimePickerDialogLogged.setMin(loggedhour, loggedminute);
//                return boundTimePickerDialogLogged;
//            case TIME_DIALOG_ID_Start:
//                // set time picker as current time
//                RangeTimePickerDialog boundTimePickerDialogStart = new RangeTimePickerDialog(this, timePickerListenerStart, starthour, startminute,
//                        true);
//                boundTimePickerDialogStart.setMin(starthour, startminute);
//                return boundTimePickerDialogStart;
//            case TIME_DIALOG_ID_End:
//                // set time picker as current time
//                RangeTimePickerDialog boundTimePickerDialogEnd = new RangeTimePickerDialog(this, timePickerListenerEnd, endhour, endminute,
//                        true);
//                boundTimePickerDialogEnd.setMin(endhour, endminute);
//                return boundTimePickerDialogEnd;
//        }
//        return null;
//    }

    private RangeTimePickerDialog.OnTimeSetListener timePickerListenerLogged = new RangeTimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            loggedhour = hourOfDay;
            loggedminute = minutes;

            updateTimeLogged(loggedhour, loggedminute);

        }

    };
    private RangeTimePickerDialog.OnTimeSetListener timePickerListenerStart = new RangeTimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            starthour = hourOfDay;
            startminute = minutes;

            updateTimeStart(starthour, startminute);

        }

    };
    private RangeTimePickerDialog.OnTimeSetListener timePickerListenerEnd = new RangeTimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            endhour = hourOfDay;
            endminute = minutes;

            updateTimeEnd(endhour, endminute);

        }

    };

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTimeLogged(int hours, int mins) {
        try {

            // Append in a StringBuilder
            String loggedTime = "";

            String loggedHours = "" + hours + ":" + mins;

            String validation = Common.checkIfTimePastOrFuture(loggedHours, LoggedHRS);

//            if (!validation.equalsIgnoreCase("Past")) {
                /////////////logged hours logic//////
                double totalMinutes = 0;
                double totalHours = 0;
                double MinutsLeft = 0;
                String[] split = loggedHours.split(":");

                try {

                    totalMinutes += Double.parseDouble(split[0]) * 60;
                    totalMinutes += Double.parseDouble(split[1]);

                    totalHours = totalMinutes / 60;
                    String FinalHours = String.valueOf(totalHours);
                    FinalHours = Common.getMeNthParamInString(FinalHours, ".", 1);
                    MinutsLeft = totalMinutes % 60;
                    if (MinutsLeft < 10) {
                        loggedTime = "0" + MinutsLeft;
                    } else {
                        loggedTime = "" + MinutsLeft;
                    }
                    String finalMintus = String.valueOf(loggedTime);
                    finalMintus = Common.getMeNthParamInString(finalMintus, ".", 1);
                    loggedTime = FinalHours + "." + finalMintus;
                } catch (Exception e) {

                }
                String startdate = mApproval_model.getStartDate();
                savedEndDate = mApproval_model.getEndDate();
                String topStartTime = convertDate(startdate);
                String topEndTime = convertDate(savedEndDate);

                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
                timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                Date date1 = timeFormat.parse(topStartTime);
                Date date2 = timeFormat.parse(loggedHours);

                long sum = date1.getTime() + date2.getTime();

                String date3 = timeFormat.format(new Date(sum));
                outputEndTime = "" + date3;

                savedEndDate = Common.getMeNthParamInString(savedEndDate, "T", 1);
                savedEndDate = savedEndDate + "T" + outputEndTime + ":00";
                mMyTextView_StartTime.setText(topStartTime);
                mMyTextView_EndTime.setText(outputEndTime);

                mMyTextView_Regular_LoggedHrs.setText(loggedTime);
//            } else {
//                SnackbarManager.show(
//                        Snackbar.with(Approval_ChangeHrs_Screen.this)
//                                .text(R.string.Time_error));
//            }

        } catch (Exception e) {

        }

    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTimeStart(int hours, int mins) {
        try {
            // Append in a StringBuilder
            String hr = "";
            String mint = "";
            if (String.valueOf(hours).length() == 1) {
                hr = "0" + hours;
            } else {
                hr = "" + hours;
            }
            if (String.valueOf(mins).length() == 1) {
                mint = "0" + mins;
            } else {
                mint = "" + mins;
            }
            String aTime = hr + ":" + mint;
            mMyTextView_StartTime.setText(aTime);
            //////////// Changing log hours
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            String endDate = mMyTextView_EndTime.getText().toString().trim();
            Date date1 = timeFormat.parse(aTime);
            Date date2 = timeFormat.parse(endDate);

            long sum = date2.getTime() - date1.getTime();

            String date3 = timeFormat.format(new Date(sum));
            String loggedHours = Common.getMeNthParamInString(date3, ":", 1);

            char first = loggedHours.charAt(0);
            String firstchar = String.valueOf(first);
            if (firstchar.equalsIgnoreCase("0")) {
                loggedHours = loggedHours.substring(1);
            }
            String loggedMintus = Common.getMeNthParamInString(date3, ":", 2);

            mMyTextView_Regular_LoggedHrs.setText(loggedHours + "." + loggedMintus);
        } catch (Exception e) {

        }
    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTimeEnd(int hours, int mins) {
        try {
            String loggedTime = "";
            String hr = "";
            String mint = "";
            if (String.valueOf(hours).length() == 1) {
                hr = "0" + hours;
            } else {
                hr = "" + hours;
            }
            if (String.valueOf(mins).length() == 1) {
                mint = "0" + mins;
            } else {
                mint = "" + mins;
            }
            String aTime = hr + ":" + mint;
            // Append in a StringBuilder
            String startTime = mMyTextView_StartTime.getText().toString().trim();
            String loggedHours = aTime;
            String validation = Common.checkIfTimePastOrFutureForLeave(loggedHours, startTime);
            if (!validation.equalsIgnoreCase("Past")) {
                //// Setting end time on screen
                mMyTextView_EndTime.setText(loggedHours);
                /////////////
                //////////// Changing log hours
                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
                timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                Date date1 = timeFormat.parse(startTime);
                Date date2 = timeFormat.parse(loggedHours);

                long sum = date2.getTime() - date1.getTime();

                String date3 = timeFormat.format(new Date(sum));
                loggedHours = date3;

                //////////////
                /////////////logged hours logic//////
                double totalMinutes = 0;
                double totalHours = 0;
                double MinutsLeft = 0;
                String[] split = loggedHours.split(":");
                try {

                    totalMinutes += Double.parseDouble(split[0]) * 60;
                    totalMinutes += Double.parseDouble(split[1]);

                    totalHours = totalMinutes / 60;
                    String FinalHours = String.valueOf(totalHours);
                    FinalHours = Common.getMeNthParamInString(FinalHours, ".", 1);
                    MinutsLeft = totalMinutes % 60;
                    if (MinutsLeft < 10) {
                        loggedTime = "0" + MinutsLeft;
                    } else {
                        loggedTime = "" + MinutsLeft;
                    }
                    String finalMintus = String.valueOf(loggedTime);
                    finalMintus = Common.getMeNthParamInString(finalMintus, ".", 1);
                    loggedTime = FinalHours + "." + finalMintus;
                    mMyTextView_Regular_LoggedHrs.setText(loggedTime);
                } catch (Exception e) {

                }
            } else {
                SnackbarManager.show(
                        Snackbar.with(Approval_ChangeHrs_Screen.this)
                                .text(R.string.approve_error_time));
            }

        } catch (Exception e) {

        }

    }

    /////////Save Logged Hours On Server
    private class SaveLoggedHoursOnServer extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(Approval_ChangeHrs_Screen.this, true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String url = Common.SERVER_URL + "Api/WorkReportItem/WorkItemAddHours";
                JSONObject ApproveStatusJsonObject = new JSONObject();
                Approval_Model approval_model = mApproval_model;
                String UserId = sharedPrefrances.getUserID(Approval_ChangeHrs_Screen.this);
                ///////////////// Getting saved User name and password
                String EnteredUserName = sharedPrefrances.getEnteredUserName(Approval_ChangeHrs_Screen.this);
                String EnteredPassword = sharedPrefrances.getEnteredPassword(Approval_ChangeHrs_Screen.this);
                ///////// Converting string values in Integer values
                int IssueId = Integer.parseInt(approval_model.getIssueId());
                String Duration = mMyTextView_Regular_LoggedHrs.getText().toString().trim();
                String comment = mEditText_comment.getText().toString().trim();
                String WorkDate = approval_model.getWorkDate();
                int IssueCommentId = Integer.parseInt(approval_model.getIssueCommentId());
                ////////// Adding data in json object
                try {
                    ApproveStatusJsonObject.put("UserId", UserId);
                    ApproveStatusJsonObject.put("IssueId", IssueId);
                    ApproveStatusJsonObject.put("WorkDate", WorkDate);
                    ApproveStatusJsonObject.put("Duration", Duration);
                    ApproveStatusJsonObject.put("Comment", comment);
                    ApproveStatusJsonObject.put("IssueCommentId", IssueCommentId);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mSaveCommentResult = Webservices.ApiCall(url, ApproveStatusJsonObject,
                        Approval_ChangeHrs_Screen.this, 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mSaveCommentResult);
                if (mSaveCommentResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mSaveCommentResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        responseResultcode = resultJsonObject.getInt("StatusCode");

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mSaveCommentResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mSaveCommentResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mSaveCommentResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (responseResultcode == 200) {
                String USerID = mApproval_model.getUserIdd();
                SkanDB.UpdatEndDateAfterSaved(USerID, savedEndDate, mMyTextView_Regular_LoggedHrs.getText().toString().trim());
                if (ApproveFlag) {
                    new SubmitApproveStatusOnServer().execute();
                } else {

                    if (Common.Fragment_No == 0) {
                        Approval_Fragment.approval_fragment.change_Approval_List(Home_Screen.formattedDateForDB);
                    } else {
                        CalendarFragment.calendarFragment.change_Approval_List();
                    }
                    finish();
                }
            } else {
                SnackbarManager.show(
                        Snackbar.with(Approval_ChangeHrs_Screen.this)
                                .text(R.string.Server_error));
            }
            mProgressHUD.dismiss();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }

    /////////Submit Approve Status On Server
    private class SubmitApproveStatusOnServer extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(Approval_ChangeHrs_Screen.this, true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String url = Common.SERVER_URL + "Api/WorkReportItem/ApproveTask";
                JSONObject ApproveStatusJsonObject = new JSONObject();
                Approval_Model approval_model = mApproval_model;
                String UserId = sharedPrefrances.getUserID(Approval_ChangeHrs_Screen.this);
                /////////// Getting current date and time
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date currentDate = new Date();
                //// Converting date to string format
                String RequestDate = df.format(currentDate);
                ///////////////// Getting Week number of year
                Calendar calender = Calendar.getInstance();
                int WeekNumber = calender.get(Calendar.WEEK_OF_YEAR);
                ///////////////// Getting saved User name and password
                String EnteredUserName = sharedPrefrances.getEnteredUserName(Approval_ChangeHrs_Screen.this);
                String EnteredPassword = sharedPrefrances.getEnteredPassword(Approval_ChangeHrs_Screen.this);
                ///////// Converting string values in Integer values
                int IssueId = Integer.parseInt(approval_model.getIssueId());
                int RequestId = Integer.parseInt(approval_model.getRequestId());
                ////////// Adding data in json object
                try {
                    ApproveStatusJsonObject.put("UserId", UserId);
                    ApproveStatusJsonObject.put("RequestDate", RequestDate);
                    ApproveStatusJsonObject.put("ApprovalStatusId", "2");
                    ApproveStatusJsonObject.put("WeekNumber", WeekNumber);//current week of the year integer data
                    ApproveStatusJsonObject.put("IssueId", IssueId);//integer
                    ApproveStatusJsonObject.put("WorkDate", approval_model.getWorkDate());
                    ApproveStatusJsonObject.put("RequestId", RequestId);//integer
                    ApproveStatusJsonObject.put("Comment", mEditText_comment.getText().toString().trim());
                    ApproveStatusJsonObject.put("SaveComment", 1);//integer // on approve screen 0 an in edit screen 1
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mSubmitApprovalResult = Webservices.ApiCall(url, ApproveStatusJsonObject,
                        Approval_ChangeHrs_Screen.this, 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mSubmitApprovalResult);
                if (mSubmitApprovalResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mSubmitApprovalResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        responseResultcode = resultJsonObject.getInt("StatusCode");

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mSubmitApprovalResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mSubmitApprovalResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mSubmitApprovalResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (responseResultcode == 200) {
                String id = mApproval_model.getUserIdd();
                SkanDB.UpdatApprovalStatusID(id, "2");
                if (Common.Fragment_No == 0) {
                    Approval_Fragment.approval_fragment.change_Approval_List(Home_Screen.formattedDateForDB);
                } else {
                    CalendarFragment.calendarFragment.change_Approval_List();
                }
                finish();
            } else {
                SnackbarManager.show(
                        Snackbar.with(Approval_ChangeHrs_Screen.this)
                                .text(R.string.Task_Server_error));
            }
            mProgressHUD.dismiss();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }

    @Override
    public void onUserInteraction() {
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentDate = new Date();
        String currentDateTimeString = df.format(currentDate);

        sharedPrefrances.saveActivityTimeStamp(Approval_ChangeHrs_Screen.this, currentDateTimeString);
        Log.e("Touching",
                "Touching done......");
    }
}
