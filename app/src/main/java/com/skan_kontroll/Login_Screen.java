package com.skan_kontroll;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.Calendar;

import Comman.Common;
import Comman.ProgressHUD;
import Comman.Webservices;
import DB.DBClass;
import DB.sharedPrefrances;


public class Login_Screen extends Activity implements View.OnClickListener {

    private EditText mEditText_UserName, mEditText_Password;
    //    private Button mButton_Login;
    private LinearLayout mLinearLayout_login;
    private String mLoginResult = "";
    private ProgressHUD mProgressHUD;
    private DBClass SkanDB;
    private String mApprovalResult = "";
    private String mLeavesResult = "";
    private boolean mloginFlag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login__screen);
        try {
            // /////Initializing  DB
            SkanDB = DBClass.getDBAdapterInstance(Login_Screen.this);
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //// Initializing all static view in this function
        init();
        ///// Setting all views click Listener In this function
        setClicksListener();
    }

    //// Initializing all static view in this function
    private void init() {
        ////// Initializing EditText view
        mEditText_UserName = (EditText) findViewById(R.id.EditText_UserName);
        mEditText_Password = (EditText) findViewById(R.id.EditText_Password);
        ////// Initializing LinearLayout view
        mLinearLayout_login = (LinearLayout) findViewById(R.id.LinearLayout_login);
    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
        mLinearLayout_login.setOnClickListener(this);
    }

    /////////Authenticating user on server
    private class callLoginApi extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(Login_Screen.this, true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String url = Common.SERVER_URL + "Api/Account";
                JSONObject loginJsonObject = new JSONObject();
                String username = mEditText_UserName.getText().toString().trim();
                String password = mEditText_Password.getText().toString().trim();
                try {
                    loginJsonObject.put("UserName", username);
                    loginJsonObject.put("Password", password);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mLoginResult = Webservices.ApiCall(url, loginJsonObject,
                        Login_Screen.this, 8000, 1, "", "");
                System.out.println("response: " + mLoginResult);
                if (mLoginResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mLoginResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        int resultcode = resultJsonObject.getInt("StatusCode");
                        if (resultcode == 200) {
                            String Model = jsonObject.getString("Model");
                            JSONObject ModelJsonObject = new JSONObject(Model);
                            String UserId = ModelJsonObject.getString("UserId");
                            String UserName = ModelJsonObject.getString("UserName");
                            String Email = ModelJsonObject.getString("Email");
                            String Firstname = ModelJsonObject.getString("Firstname");
                            String LastName = ModelJsonObject.getString("LastName");
                            String DisplayName = ModelJsonObject.getString("DisplayName");
                            sharedPrefrances.SaveFirstRun(Login_Screen.this, 1);
                            sharedPrefrances.saveUserID(Login_Screen.this, UserId);
                            sharedPrefrances.saveUserName(Login_Screen.this, UserName);
                            sharedPrefrances.saveUserEmail(Login_Screen.this, Email);
                            sharedPrefrances.saveUserFirstname(Login_Screen.this, Firstname);
                            sharedPrefrances.saveUserLastName(Login_Screen.this, LastName);
                            sharedPrefrances.saveUserDisplayName(Login_Screen.this, DisplayName);
                            sharedPrefrances.saveEnteredPassword(Login_Screen.this, password);
                            sharedPrefrances.saveEnteredUserName(Login_Screen.this, username);
                            SnackbarManager.show(
                                    Snackbar.with(Login_Screen.this)
                                            .text(R.string.Login_Success));
                            mloginFlag = true;

                        } else {
                            SnackbarManager.show(
                                    Snackbar.with(Login_Screen.this)
                                            .text(R.string.Login_error));
                            mloginFlag = false;
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mLoginResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mLoginResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mLoginResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            mProgressHUD.dismiss();
            if (mloginFlag) {
                if (Webservices.isInternetOn(Login_Screen.this)) {
                    /////////Getting Approval data from server
                    new callApprovalApi().execute();
                }
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }

    /////////Getting Approval data from server
    private class callApprovalApi extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(Login_Screen.this, true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String url = Common.SERVER_URL + "Api/CustomerArticleDetail/GetAllArticleDetails";
                JSONObject ApprovalJsonObject = new JSONObject();
                String UserId = sharedPrefrances.getUserID(Login_Screen.this);
                String UserName = sharedPrefrances.getUserName(Login_Screen.this);
                String EnteredUserName = sharedPrefrances.getEnteredUserName(Login_Screen.this);
                String EnteredPassword = sharedPrefrances.getEnteredPassword(Login_Screen.this);
                try {
                    ApprovalJsonObject.put("UserId", UserId);
                    ApprovalJsonObject.put("UserName", UserName);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mApprovalResult = Webservices.ApiCall(url, ApprovalJsonObject,
                        Login_Screen.this, 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mApprovalResult);
                if (mApprovalResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mApprovalResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        int resultcode = resultJsonObject.getInt("StatusCode");
                        if (resultcode == 200) {
                            //// Delete all previous records
                            SkanDB.deleteApprovalDataInDB();
                            JSONArray modelJsonArray = jsonObject
                                    .getJSONArray("Models");
                            if (modelJsonArray.length() > 0) {
                                /////Inserting bulk data to local DB
                                SkanDB.saveApprovalDataInDB(modelJsonArray);
                            }

                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mApprovalResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mApprovalResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mApprovalResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            new GetLeavesDataFromServer().execute();
            mProgressHUD.dismiss();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }

    /////////Getting Leaves Data From Server
    private class GetLeavesDataFromServer extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(Login_Screen.this, true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                int year = Calendar.getInstance().get(Calendar.YEAR);
                String url = Common.SERVER_URL + "Api/Leave/GetUserLeaves";
                JSONObject LeaveJsonObject = new JSONObject();
                String UserName = sharedPrefrances.getUserName(Login_Screen.this);
                String EnteredUserName = sharedPrefrances.getEnteredUserName(Login_Screen.this);
                String EnteredPassword = sharedPrefrances.getEnteredPassword(Login_Screen.this);
                try {
                    LeaveJsonObject.put("OrganizationId", "12");
                    LeaveJsonObject.put("UserName", UserName);
                    LeaveJsonObject.put("LeaveStartDate", "01/01/" + year);
                    LeaveJsonObject.put("LeaveEndDate", "12/31/" + year);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mLeavesResult = Webservices.ApiCall(url, LeaveJsonObject,
                        Login_Screen.this, 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mLeavesResult);
                if (mLeavesResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mLeavesResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        int resultcode = resultJsonObject.getInt("StatusCode");
                        if (resultcode == 200) {
                            //// Delete all previous records
                            SkanDB.deleteLeaveDataInDB();
                            JSONArray modelJsonArray = jsonObject
                                    .getJSONArray("Models");
                            if (modelJsonArray.length() > 0) {
                                /////Inserting bulk data to local DB
                                SkanDB.saveLeavesDataInDB(modelJsonArray);
                            }
                            Common.Leave_API_Flag = false;
                            startService(new Intent(Login_Screen.this, Logout_Service.class));
                            Intent i = new Intent(Login_Screen.this, Home_Screen.class);
                            startActivity(i);
                            finish();
                        } else {
                            startService(new Intent(Login_Screen.this, Logout_Service.class));
                            Intent i = new Intent(Login_Screen.this, Home_Screen.class);
                            startActivity(i);
                            finish();
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        mProgressHUD.dismiss();
                        startService(new Intent(Login_Screen.this, Logout_Service.class));
                        Intent i = new Intent(Login_Screen.this, Home_Screen.class);
                        startActivity(i);
                        finish();
                    }

                } else {
                    startService(new Intent(Login_Screen.this, Logout_Service.class));
                    Intent i = new Intent(Login_Screen.this, Home_Screen.class);
                    startActivity(i);
                    finish();
                }
            } catch (SocketTimeoutException bug) {
                mLeavesResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mLeavesResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mLeavesResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            mProgressHUD.dismiss();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }

    private boolean validations() {
        boolean result = true;
        if (mEditText_UserName.getText().toString().trim().length() <= 0) {
            SnackbarManager.show(
                    Snackbar.with(Login_Screen.this)
                            .text(R.string.Blank_userName));
            return result = false;
        } else if (mEditText_Password.getText().toString().trim().length() <= 0) {
            SnackbarManager.show(
                    Snackbar.with(Login_Screen.this)
                            .text(R.string.Blank_Psssword));
            return result = false;
        }
        return result;
    }

    @Override
    public void onClick(View view) {
        if (mLinearLayout_login == view) {
            //// Checking for fields validations
            if (validations()) {
                ////Checking if internet is available or not
                if (Webservices.isInternetOn(Login_Screen.this)) {
                    /////////Authenticating user on server
                    new callLoginApi().execute();
                } else {
                    SnackbarManager.show(
                            Snackbar.with(Login_Screen.this)
                                    .text(R.string.Internet_error));
                }
            }

        }
    }
}
