package com.skan_kontroll;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import Comman.DirectionsJSONParser;
import Comman.GPSTracker;
import Comman.MyTextView_Regular;
import Comman.ProgressHUD;
import Comman.Webservices;
import DB.sharedPrefrances;
import Model.Approval_Model;

public class Map_route_Screen extends FragmentActivity implements View.OnClickListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private LinearLayout mLinearLayout_Back;
    private MyTextView_Regular mMyTextView_Regular_Address;
    private ArrayList<LatLng> markerPoints;
    private String googleAPI_Response = "";
    private ProgressHUD mProgressHUD;
    private Approval_Model mApproval_model;
    private String finalAddress = "";
    private LocationManager lm;
    private static Location location;
    private String CurrentLat = "0.0";
    private String CurrentLon = "0.0";
    private double source_lat = 0.0;
    private double source_lng = 0.0;
    final private int Request_Location = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_route__screen);
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            String beforeAsked = sharedPrefrances.getMapPermissions(Map_route_Screen.this);
            if (beforeAsked == null || beforeAsked.equalsIgnoreCase("0")) {
                // only for gingerbread and newer versions
                ActivityCompat.requestPermissions(Map_route_Screen.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, Request_Location);
            } else {
                init();
                setClicksListener();
                GetLatAndLong();
                setUpMapIfNeeded();
                if (!(lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm
                        .isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
                    // dialog shown when location service is of
                    LocationDialog();
                } else {
                    ////Checking if internet is available or not
                    if (Webservices.isInternetOn(Map_route_Screen.this)) {
                        /////////Getting lat long using reverse GeoCoding API
                        new GetAddressLatLong().execute();
                    }
                }
            }
        } else {
            init();
            setClicksListener();
            GetLatAndLong();
            setUpMapIfNeeded();
            if (!(lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
                // dialog shown when location service is of
                LocationDialog();
            } else {
                ////Checking if internet is available or not
                if (Webservices.isInternetOn(Map_route_Screen.this)) {
                    /////////Getting lat long using reverse GeoCoding API
                    new GetAddressLatLong().execute();
                }
            }
        }
        //// change top status bar color same as Action bar color
        changeStatusBarColor();
    }
    //// change top status bar color same as Action bar color
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = Map_route_Screen.this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(Map_route_Screen.this.getResources().getColor(R.color.Status_Bar_Color));
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Request_Location:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sharedPrefrances.saveMapPermissions(Map_route_Screen.this, "1");
                    init();
                    setClicksListener();
                    GetLatAndLong();
                    setUpMapIfNeeded();
                    if (!(lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm
                            .isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
                        // dialog shown when location service is of
                        LocationDialog();
                    } else {
                        ////Checking if internet is available or not
                        if (Webservices.isInternetOn(Map_route_Screen.this)) {
                            /////////Getting lat long using reverse GeoCoding API
                            new GetAddressLatLong().execute();
                        }
                    }
                } else {
                    sharedPrefrances.saveMapPermissions(Map_route_Screen.this, "0");
                    SnackbarManager.show(
                            Snackbar.with(Map_route_Screen.this)
                                    .text(R.string.Denied_Permission_error));
                }

                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
//            if (mMap != null) {
//                setUpMap(0.0, 0.0);
//            }
        }
    }

    // dialog shown when location service is of
    private void LocationDialog() {
        try {
            AlertDialog.Builder alert_box = new AlertDialog.Builder(Map_route_Screen.this);
            alert_box.setTitle(R.string.Alert);
            alert_box.setCancelable(false);
            alert_box.setMessage(R.string.Location_msg);
            alert_box.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    ///////// User will be taken to location setting screen
                    startActivityForResult(
                            new Intent(
                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                            29);

                }
            });
//        alert_box.setNegativeButton("No", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//
//            }
//        });

            AlertDialog dialog = alert_box.create();
            dialog.show();
            //// /changing Positive button color runtime
            Button b1 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            if (b1 != null)
                b1.setTextColor(getResources().getColor(R.color.pink_Color));
        } catch (Exception e) {
            // Tracking exception

        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap(double source_lat, double source_lng, double destination_lat, double destination_lng) {
        if (source_lat != 0.0 && source_lng != 0.0) {
            //////// Adding Source lat long and destination lat long n array
            markerPoints.add(new LatLng(source_lat, source_lng));
            markerPoints.add(new LatLng(destination_lat, destination_lng));
            ////////////// Getting data from model class
            String address = mApproval_model.getAddress();
            String street = mApproval_model.getStreet();
            String visitCityPostalCode = mApproval_model.getVisitCityPostalCode();
            String city = mApproval_model.getCity();
            //////////////
            MarkerOptions options = new MarkerOptions();
            // Setting the position of the marker
            options.position(new LatLng(source_lat, source_lng));
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mMap.addMarker(options).showInfoWindow();
            MarkerOptions options2 = new MarkerOptions();
            options2.position(new LatLng(destination_lat, destination_lng)).title(address)
                    .snippet(address + "\n" + street + "\n" + visitCityPostalCode + "\n" + city);
            options2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mMap.addMarker(options2).showInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destination_lat, destination_lng), 11.0f));
            // Checks, whether start and end locations are captured
            if (markerPoints.size() >= 2) {
                LatLng origin = markerPoints.get(0);
                LatLng dest = markerPoints.get(1);

                // Getting URL to the Google Directions API
                String url = getDirectionsUrl(origin, dest);

                DownloadTask downloadTask = new DownloadTask();

                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }
        }
    }

    //// Initializing all static view in this function
    private void init() {
        // Initializing
        // 1. get passed intent
        Intent intent = getIntent();
        // 2. get Approval_Model object from intent which is sent from previous activity
        mApproval_model = (Approval_Model) intent.getSerializableExtra("Address");
        ///////////////////////
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //////
        markerPoints = new ArrayList<LatLng>();
        ////////////////
        mLinearLayout_Back = (LinearLayout) findViewById(R.id.LinearLayout_Back);
        ////////////
        String address = mApproval_model.getAddress();
        mMyTextView_Regular_Address = (MyTextView_Regular) findViewById(R.id.MyTextView_Address);
        mMyTextView_Regular_Address.setText(address);
    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
        mLinearLayout_Back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mLinearLayout_Back) {
            finish();
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);

                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(20);
                lineOptions.color(Color.RED);

            }
            if (lineOptions != null) {
                // Drawing polyline in the Google Map for the i-th route
                mMap.addPolyline(lineOptions);
            }
        }
    }

    /////////Getting lat long using reverse GeoCoding API
    private class GetAddressLatLong extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(Map_route_Screen.this, true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String address = mApproval_model.getAddress();
                String street = mApproval_model.getStreet();
                String visitCityPostalCode = mApproval_model.getVisitCityPostalCode();
                String city = mApproval_model.getCity();
                finalAddress = address + " " + street + " " + visitCityPostalCode + " " + city;

                StringBuilder stringBuilder = new StringBuilder();
                try {
                    finalAddress = finalAddress.replaceAll(" ", "%20");
                    //// hitting google API to get user current address on basis on user lat and long
                    String url = "http://maps.googleapis.com/maps/api/geocode/json?address="
                            + finalAddress + "&sensor=false";
                    HttpPost httppost = new HttpPost(url);
                    HttpClient client = new DefaultHttpClient();
                    HttpResponse response;
                    stringBuilder = new StringBuilder();


                    response = client.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    InputStream stream = entity.getContent();
                    int b;
                    while ((b = stream.read()) != -1) {
                        stringBuilder.append((char) b);
                    }
                } catch (ClientProtocolException e) {
                } catch (IOException e) {
                }
                googleAPI_Response = "";
                googleAPI_Response = stringBuilder.toString();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {
                if (googleAPI_Response.length() > 0) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject = new JSONObject(googleAPI_Response);

                        double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                                .getJSONObject("geometry").getJSONObject("location")
                                .getDouble("lng");

                        double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                                .getJSONObject("geometry").getJSONObject("location")
                                .getDouble("lat");
                        setUpMap(source_lat, source_lng, lat, lng);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        setUpMap(0.0, 0.0, 0.0, 0.0);
                        mProgressHUD.dismiss();
                    }
                } else {
                    setUpMap(0.0, 0.0, 0.0, 0.0);
                }
                mProgressHUD.dismiss();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);

            mProgressHUD.dismiss();
        }
    }

    ////// Getting users current lat and long using GPS
    private void GetLatAndLong() {
        GPSTracker gpsTracker = new GPSTracker(Map_route_Screen.this);
        if (gpsTracker.canGetLocation()) {
            try {
                // getting lat from gpsTracker
                String stringLatitude = String.valueOf(gpsTracker.latitude);
                // getting long from gpsTracker
                String stringLongitude = String.valueOf(gpsTracker.longitude);
                CurrentLat = stringLatitude;
                CurrentLon = stringLongitude;
                if (CurrentLat.length() > 0) {
                    source_lat = Double.parseDouble(CurrentLat);
                }
                if (CurrentLat.length() > 0) {
                    source_lng = Double.parseDouble(CurrentLon);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            ////////if user is not getting lat and long using GPS then will try to get using location manager
            try {
                LocationManager mngr = (LocationManager) getSystemService(LOCATION_SERVICE);
                Criteria criteria = new Criteria();
                String best = mngr.getBestProvider(criteria, true);
                location = mngr.getLastKnownLocation(best);
                // getting lat from Location manager
                String stringLatitude = String.valueOf(location.getLatitude());
                // getting long from Location manager
                String stringLongitude = String
                        .valueOf(location.getLongitude());
                CurrentLat = stringLatitude;
                CurrentLon = stringLongitude;
                if (CurrentLat.length() > 0) {
                    source_lat = Double.parseDouble(CurrentLat);
                }
                if (CurrentLat.length() > 0) {
                    source_lng = Double.parseDouble(CurrentLon);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    @Override
    public void onUserInteraction() {
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentDate = new Date();
        String currentDateTimeString = df.format(currentDate);

        sharedPrefrances.saveActivityTimeStamp(Map_route_Screen.this, currentDateTimeString);
        Log.e("Touching",
                "Touching done......");
    }
}
