package com.skan_kontroll;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import DB.DBClass;
import DB.sharedPrefrances;

public class Logout_Service extends Service {

    private DBClass SkanDB;
    int delay = 0; // delay for 1 sec.
    int period = 600000; // repeat every 10 mints.
    Timer timer = null;
//    int period = 10000; // repeat every 10 mints.

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
        Log.d("hello", "LOgoutService started");
        try {
            // /////Initializing  DB
            SkanDB = DBClass.getDBAdapterInstance(this);
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                try {
                    Date compairdate = null;
                    String storedTimeStamp = sharedPrefrances.getActivityTimeStamp(Logout_Service.this);
                    if (storedTimeStamp == null) {
                        storedTimeStamp = "";
                    }
                    if (storedTimeStamp.length() > 0) {

                        //Format of the date defined in the input String
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date currentDate = new Date();
                        //Converting the input String to Date
                        compairdate = df.parse(storedTimeStamp);

                        long mills = currentDate.getTime() - compairdate.getTime();
//                    int Hours = (int) (mills / (1000 * 60 * 60));
                        int Mins = (int) (mills / (1000 * 60)) % 60;
                        if (Mins >= 30) {
                            Logout();
                        }
                    }

                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }, delay, period);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d("hello destroy", "LogoutService destroyed");
    }

    private void Logout() {
        ///////// user user selected yes then making changes in DB accordingly
        ////////////// Deleting user data
        sharedPrefrances.SaveFirstRun(this, 0);
        sharedPrefrances.saveUserID(this, "");
        sharedPrefrances.saveUserName(this, "");
        sharedPrefrances.saveUserEmail(this, "");
        sharedPrefrances.saveUserFirstname(this, "");
        sharedPrefrances.saveUserLastName(this, "");
        sharedPrefrances.saveUserDisplayName(this, "");
        sharedPrefrances.saveEnteredPassword(this, "");
        sharedPrefrances.saveEnteredUserName(this, "");
        sharedPrefrances.saveActivityTimeStamp(this,"");
        SkanDB.deleteApprovalDataInDB();
        SkanDB.deleteLeaveDataInDB();
        Log.e("Logout runnable called>>>>>>",
                "Logout done......");
        stopService(new Intent(this, Logout_Service.class));
        timer.cancel();
        // System.exit(0);
    }

}