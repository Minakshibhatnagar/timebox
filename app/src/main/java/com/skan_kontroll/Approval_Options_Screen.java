package com.skan_kontroll;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import Comman.MyTextView_Regular;
import DB.DBClass;
import DB.sharedPrefrances;
import Model.Approval_Model;


public class Approval_Options_Screen extends Activity implements View.OnClickListener {
    private DBClass SkanDB;
    private LinearLayout mLinearLayout_CustAddress, mLinearLayout_CustInfo, mLinearLayout_TaskInfo, mLinearLayout_Map, mLinearLayout_Back;
    private ImageView mImageView_CustInfoIcon, mImageView_CustInfoBack, mImageView_TaskInfoIcon, mImageView_TaskInfoBack;
    private MyTextView_Regular mMyTextView_CustInfoTextRegular, mMyTextView_Regular_TaskInfo, mMyTextView_Regular_Address;
    private Approval_Model mApproval_model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approval_options);
        try {
            // /////Initializing  DB
            SkanDB = DBClass.getDBAdapterInstance(this);
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // 1. get passed intent
        Intent intent = getIntent();
        // 2. get Approval_Model object from intent which is sent from previous activity
        mApproval_model = (Approval_Model) intent.getSerializableExtra("ModelClass");
        //// Initializing all static view in this function
        init();
        ///// Setting all views click Listener In this function
        setClicksListener();
        //// change top status bar color same as Action bar color
        changeStatusBarColor();
    }
    //// change top status bar color same as Action bar color
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = Approval_Options_Screen.this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(Approval_Options_Screen.this.getResources().getColor(R.color.Status_Bar_Color));
        }
    }

    //// Initializing all static view in this function
    private void init() {
        mLinearLayout_CustAddress = (LinearLayout) findViewById(R.id.LinearLayout_CustAddress);
        mLinearLayout_CustInfo = (LinearLayout) findViewById(R.id.LinearLayout_CustInfo);
        mLinearLayout_TaskInfo = (LinearLayout) findViewById(R.id.LinearLayout_TaskInfo);
        mLinearLayout_Map = (LinearLayout) findViewById(R.id.LinearLayout_Map);
        mLinearLayout_Back = (LinearLayout) findViewById(R.id.LinearLayout_Back);
        /////////////////////////
        mImageView_CustInfoIcon = (ImageView) findViewById(R.id.ImageView_CustInfoIcon);
        mImageView_CustInfoBack = (ImageView) findViewById(R.id.ImageView_CustInfoBack);
        mImageView_TaskInfoIcon = (ImageView) findViewById(R.id.ImageView_TaskInfoIcon);
        mImageView_TaskInfoBack = (ImageView) findViewById(R.id.ImageView_TaskInfoBack);
        ///////////////////
        mMyTextView_CustInfoTextRegular = (MyTextView_Regular) findViewById(R.id.MyTextView_CustInfoText);
        mMyTextView_Regular_TaskInfo = (MyTextView_Regular) findViewById(R.id.MyTextView_TaskInfo);
        mMyTextView_Regular_Address = (MyTextView_Regular) findViewById(R.id.MyTextView_Address);
        String address = mApproval_model.getAddress();
        mMyTextView_Regular_Address.setText(address);

        String CustomerComment = mApproval_model.getCustomerComment().trim();
        String SchedulerComment = mApproval_model.getSchedulerComment().trim();
        if (CustomerComment.length() == 0 || CustomerComment == null || CustomerComment.equalsIgnoreCase("null")) {
            mMyTextView_CustInfoTextRegular.setTextColor(Color.parseColor("#808080"));
        } else {
            mMyTextView_CustInfoTextRegular.setTextColor(Color.parseColor("#000000"));
        }

        if (SchedulerComment.length() == 0 || SchedulerComment == null || SchedulerComment.equalsIgnoreCase("null")) {
            mMyTextView_Regular_TaskInfo.setTextColor(Color.parseColor("#808080"));
        } else {
            mMyTextView_Regular_TaskInfo.setTextColor(Color.parseColor("#000000"));
        }

    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
        mLinearLayout_CustAddress.setOnClickListener(this);
        mLinearLayout_CustInfo.setOnClickListener(this);
        mLinearLayout_TaskInfo.setOnClickListener(this);
        mLinearLayout_Map.setOnClickListener(this);
        mLinearLayout_Back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mLinearLayout_CustAddress) {
            String address = mApproval_model.getAddress().trim();
            String street = mApproval_model.getStreet().trim();
            if (address.length() > 0 && address != null) {
                Intent intent = new Intent(Approval_Options_Screen.this, Current_Address_Map_Screen.class);
                intent.putExtra("Address", mApproval_model);
                startActivity(intent);
            }
        } else if (view == mLinearLayout_CustInfo) {
            String CustomerComment = mApproval_model.getCustomerComment().trim();
            String address = mApproval_model.getAddress().trim();
            if (CustomerComment.length() != 0 && CustomerComment != null && !CustomerComment.equalsIgnoreCase("null")) {
                Intent intent = new Intent(Approval_Options_Screen.this, Cust_Task_info_Screen.class);
                intent.putExtra("CustomerComment", CustomerComment);
                intent.putExtra("Address", address);
                startActivity(intent);
            }
        } else if (view == mLinearLayout_TaskInfo) {
            String SchedulerComment = mApproval_model.getSchedulerComment().trim();
            String address = mApproval_model.getAddress().trim();
            if (SchedulerComment.length() != 0 && SchedulerComment != null && !SchedulerComment.equalsIgnoreCase("null")) {
                Intent intent = new Intent(Approval_Options_Screen.this, Cust_Task_info_Screen.class);
                intent.putExtra("CustomerComment", SchedulerComment);
                intent.putExtra("Address", address);
                startActivity(intent);
            }
        } else if (view == mLinearLayout_Map) {
            String address = mApproval_model.getAddress().trim();
            String street = mApproval_model.getStreet().trim();
            if (address.length() > 0 && address != null) {
                Intent intent = new Intent(Approval_Options_Screen.this, Map_route_Screen.class);
                intent.putExtra("Address", mApproval_model);
                startActivity(intent);
            }
        } else if (view == mLinearLayout_Back) {
            finish();
        }
    }
    @Override
    public void onUserInteraction() {
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentDate = new Date();
        String currentDateTimeString = df.format(currentDate);

        sharedPrefrances.saveActivityTimeStamp(Approval_Options_Screen.this, currentDateTimeString);
        Log.e("Touching",
                "Touching done......");
    }
}
