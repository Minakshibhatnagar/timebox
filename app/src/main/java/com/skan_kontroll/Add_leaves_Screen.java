package com.skan_kontroll;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import Comman.Common;
import Comman.MyTextView_Regular;
import Comman.ProgressHUD;
import Comman.Webservices;
import DB.DBClass;
import DB.sharedPrefrances;
import Fragment.Leave_Fragment;


public class Add_leaves_Screen extends Activity implements View.OnClickListener {
    private DBClass SkanDB;
    private LinearLayout mLinearLayout_From, mLinearLayout_To, mLinearLayout_leaveType, mLinearLayout_Back;
    private MyTextView_Regular mMyTextView_Regular_From, mMyTextView_Regular_To, mMyTextView_Regular_LeaveType;
    private Button mButton_Cancel, mButton_Apply_leave;
    private EditText mEditText_comment;
    private int year;
    private int month;
    private int day;
    private int year2;
    private int month2;
    private int day2;
    static final int DATE_PICKER_ID = 1111;
    static final int DATE_PICKER_ID2 = 2222;
    private ProgressHUD mProgressHUD;
    private String mSaveLeaveResult = "";
    private int leave_id = 0;
    private boolean saveFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_leaves__screen);
        try {
            // /////Initializing  DB
            SkanDB = DBClass.getDBAdapterInstance(this);
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //// Initializing all static view in this function
        init();
        ///// Setting all views click Listener In this function
        setClicksListener();
       //// change top status bar color same as Action bar color
        changeStatusBarColor();
    }
    //// change top status bar color same as Action bar color
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = Add_leaves_Screen.this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(Add_leaves_Screen.this.getResources().getColor(R.color.Status_Bar_Color));
        }
    }
    //// Initializing all static view in this function
    private void init() {
        mLinearLayout_From = (LinearLayout) findViewById(R.id.LinearLayout_From);
        mLinearLayout_To = (LinearLayout) findViewById(R.id.LinearLayout_To);
        mLinearLayout_leaveType = (LinearLayout) findViewById(R.id.LinearLayout_leaveType);
        mLinearLayout_Back = (LinearLayout) findViewById(R.id.LinearLayout_Back);
        ////////////////////
        mMyTextView_Regular_From = (MyTextView_Regular) findViewById(R.id.MyTextView_Regular_From);
        mMyTextView_Regular_To = (MyTextView_Regular) findViewById(R.id.MyTextView_Regular_To);
        mMyTextView_Regular_LeaveType = (MyTextView_Regular) findViewById(R.id.MyTextView_Regular_LeaveType);
        ///////////////////
        mButton_Cancel = (Button) findViewById(R.id.Button_Cancel);
        mButton_Apply_leave = (Button) findViewById(R.id.Button_Apply_leave);
        /////////
        mEditText_comment = (EditText) findViewById(R.id.EditText_comment);
        //////////////////
        // Get current date by calender

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

    }

    private void LeaveTypePOPUP() {
        try {
            final Dialog LeaveTypePopupView = new Dialog(Add_leaves_Screen.this);
            LeaveTypePopupView.requestWindowFeature(Window.FEATURE_NO_TITLE);
            LeaveTypePopupView.getWindow().setBackgroundDrawableResource(
                    android.R.color.transparent);
            LeaveTypePopupView.setContentView(R.layout.leave_type_popup);
            LeaveTypePopupView.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LeaveTypePopupView.getWindow().setGravity(Gravity.CENTER);
            LeaveTypePopupView.setCancelable(true);
            LeaveTypePopupView.setCanceledOnTouchOutside(false);

            ////////////////
            LinearLayout LinearLayout_three = (LinearLayout) LeaveTypePopupView
                    .findViewById(R.id.LinearLayout_three);
            LinearLayout_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    leave_id = 3;
                    mMyTextView_Regular_LeaveType.setText(R.string.sick_leave);
                    LeaveTypePopupView.dismiss();
                }
            });

            LinearLayout LinearLayout_Four = (LinearLayout) LeaveTypePopupView
                    .findViewById(R.id.LinearLayout_Four);

            LinearLayout_Four.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    leave_id = 4;
                    mMyTextView_Regular_LeaveType.setText(R.string.sick_child);
                    LeaveTypePopupView.dismiss();
                }
            });
            LinearLayout LinearLayout_TwentyEight = (LinearLayout) LeaveTypePopupView
                    .findViewById(R.id.LinearLayout_TwentyEight);
            LinearLayout_TwentyEight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    leave_id = 28;
                    mMyTextView_Regular_LeaveType.setText(R.string.vacation);
                    LeaveTypePopupView.dismiss();
                }
            });

            LeaveTypePopupView.show();
        } catch (Exception e) {
            // Tracking exception
        }
    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
        mLinearLayout_From.setOnClickListener(this);
        mLinearLayout_To.setOnClickListener(this);
        mLinearLayout_Back.setOnClickListener(this);
        mLinearLayout_leaveType.setOnClickListener(this);
        mButton_Cancel.setOnClickListener(this);
        mButton_Apply_leave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mLinearLayout_From) {
           // On button click show datepicker dialog
            showDialog(DATE_PICKER_ID);
        } else if (view == mLinearLayout_To) {
            showDialog(DATE_PICKER_ID2);
        } else if (view == mLinearLayout_leaveType) {
            LeaveTypePOPUP();
        } else if (view == mButton_Cancel || view == mLinearLayout_Back) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            finish();
        } else if (view == mButton_Apply_leave) {
            if (validations()) {
                if (Webservices.isInternetOn(Add_leaves_Screen.this)) {
                    /////////Saving leave on server
                    new SaveLeaveAPI().execute();
                } else {
                    SnackbarManager.show(
                            Snackbar.with(Add_leaves_Screen.this)
                                    .text(R.string.Internet_error));
                }
            }
        }
    }

    private boolean validations() {
        boolean result = true;
        if (mMyTextView_Regular_From.getText().toString().equalsIgnoreCase(getResources().getString(R.string.From))) {
            SnackbarManager.show(
                    Snackbar.with(Add_leaves_Screen.this)
                            .text(R.string.apply_leave_validation));
            return result = false;
        } else if (mMyTextView_Regular_To.getText().toString().equalsIgnoreCase(getResources().getString(R.string.to))) {
            SnackbarManager.show(
                    Snackbar.with(Add_leaves_Screen.this)
                            .text(R.string.apply_leave_validation));
            return result = false;
        } else if (mMyTextView_Regular_LeaveType.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Leavee_type))) {
            SnackbarManager.show(
                    Snackbar.with(Add_leaves_Screen.this)
                            .text(R.string.apply_leave_validation));
            return result = false;
        }
//        else if (mEditText_comment.getText().toString().length() <= 0) {
//            SnackbarManager.show(
//                    Snackbar.with(Add_leaves_Screen.this)
//                            .text(R.string.apply_leave_validation));
//            return result = false;
//        }
        return result;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                DatePickerDialog dpd = new DatePickerDialog(this, from_PickerListener, year, month, day);
//                Date d = new Date();
//                dpd.getDatePicker().setMinDate(d.getTime());
                return dpd;
            case DATE_PICKER_ID2:

                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
//                int months = month;
//                months = months + 1;
//                Date d2 = null;
                DatePickerDialog dpd2 = new DatePickerDialog(this, to_PickerListener, year, month, day);
//                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//                try {
//                    d2 = format.parse(day + "/" + months + "/" + year);
//                } catch (ParseException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//                dpd2.getDatePicker().setMinDate(d2.getTime());
                return dpd2;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener from_PickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            String check = Common.checkDateForLeave(day + "/" + month + 1 + "/" + year);
            if (check.equalsIgnoreCase("Past")) {
                SnackbarManager.show(
                        Snackbar.with(Add_leaves_Screen.this)
                                .text(R.string.Leave_prevoius_Date));
            } else {
                // Show selected date
                mMyTextView_Regular_From.setText(new StringBuilder().append(day)
                        .append("/").append(month + 1).append("/").append(year)
                        .append(" "));
                mMyTextView_Regular_To.setText(new StringBuilder().append(day)
                        .append("/").append(month + 1).append("/").append(year)
                        .append(" "));
            }
        }
    };
    private DatePickerDialog.OnDateSetListener to_PickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year2 = selectedYear;
            month2 = selectedMonth;
            day2 = selectedDay;

            String check = Common.checkToDateLeave(day + "/" + month + 1 + "/" + year, day2 + "/" + month2 + 1 + "/" + year2);
            if (check.equalsIgnoreCase("Past")) {
                SnackbarManager.show(
                        Snackbar.with(Add_leaves_Screen.this)
                                .text(R.string.to_Date_error));
            } else {
                mMyTextView_Regular_To.setText(new StringBuilder().append(day2)
                        .append("/").append(month2 + 1).append("/").append(year2)
                        .append(" "));
            }
        }
    };

    ///////// Calling delete API on server
    private class SaveLeaveAPI extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(Add_leaves_Screen.this, true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Format of the date defined in the input String

                String url = Common.SERVER_URL + "Api/Leave/SaveUserLeaves";
                JSONObject LeaveJsonObject = new JSONObject();
                String UserName = sharedPrefrances.getUserName(Add_leaves_Screen.this);
                String UserID = sharedPrefrances.getUserID(Add_leaves_Screen.this);
                String Leave_Comment = mEditText_comment.getText().toString().trim();
                String startDateStr = mMyTextView_Regular_From.getText().toString().trim();
                String endDateStr = mMyTextView_Regular_To.getText().toString().trim();

                //// Convert dates back to string format
                startDateStr = Common.getMeNthParamInString(startDateStr, "/", 2) + "/" + Common.getMeNthParamInString(startDateStr, "/", 1) + "/" + Common.getMeNthParamInString(startDateStr, "/", 3);
                endDateStr = Common.getMeNthParamInString(endDateStr, "/", 2) + "/" + Common.getMeNthParamInString(endDateStr, "/", 1) + "/" + Common.getMeNthParamInString(endDateStr, "/", 3);
                String EnteredUserName = sharedPrefrances.getEnteredUserName(Add_leaves_Screen.this);
                String EnteredPassword = sharedPrefrances.getEnteredPassword(Add_leaves_Screen.this);
                try {
                    LeaveJsonObject.put("LeaveStartDateString", startDateStr);
                    LeaveJsonObject.put("LeaveEndDateString", endDateStr);
                    LeaveJsonObject.put("UserName", UserName);
                    LeaveJsonObject.put("LeaveId", leave_id);
                    LeaveJsonObject.put("OrganizationId", 12);
                    LeaveJsonObject.put("LeaveComment", Leave_Comment);
                    LeaveJsonObject.put("UserId", UserID);
                    LeaveJsonObject.put("ApprovalStatusId", 2);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

//                url = Common.urlEncodeForMsg(url);
                /////////// Will get server response here
                mSaveLeaveResult = Webservices.ApiCall(url, LeaveJsonObject,
                        Add_leaves_Screen.this, 12000, 0, EnteredUserName, EnteredPassword);
                System.out.println("response: " + mSaveLeaveResult);
                if (mSaveLeaveResult.length() > 0) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(mSaveLeaveResult);
                        String ResponseText = jsonObject.getString("HttpResponseMessage");
                        JSONObject resultJsonObject = new JSONObject(ResponseText);
                        int resultcode = resultJsonObject.getInt("StatusCode");
                        if (resultcode == 200) {
                            saveFlag = true;
                        } else {
                            saveFlag = false;
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } catch (SocketTimeoutException bug) {
                mSaveLeaveResult = "";
                System.out.println("OfferServerResult SocketTimeoutException");
                bug.printStackTrace();
            } catch (ConnectTimeoutException bug) {
                mSaveLeaveResult = "";
                System.out.println("OfferServerResult ConnectTimeoutException");
                bug.printStackTrace();
            } catch (Exception e) {
                // OfferServerResult Auto-generated catch block
                mSaveLeaveResult = "";
                System.out.println("OfferServerResult " + e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (saveFlag) {
                String startDt = mMyTextView_Regular_From.getText().toString().trim();
                String endDt = mMyTextView_Regular_To.getText().toString().trim();
                String leaveType = mMyTextView_Regular_LeaveType.getText().toString().trim();
                String msg = getResources().getString(R.string.apply_leave_success) + "\n" + getResources().getString(R.string.period) + ": " + startDt + " - " + endDt + "\n" + getResources().getString(R.string.Leavee_type) + ": " + leaveType;
                LeaveSuccessDialog(msg);
            } else {
                SnackbarManager.show(
                        Snackbar.with(Add_leaves_Screen.this)
                                .text(R.string.apply_leave_error));
            }
            mProgressHUD.dismiss();
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);
            mProgressHUD.dismiss();
        }
    }

    // dialog shown to Leave Success
    private void LeaveSuccessDialog(String msg) {
        try {
            AlertDialog.Builder alert_box = new AlertDialog.Builder(Add_leaves_Screen.this);
//            alert_box.setTitle(R.string.Leave_Dilaog_titel);
            alert_box.setCancelable(false);
            alert_box.setMessage(msg);
            alert_box.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    Leave_Fragment.leave_fragment.callDataRefersh();
                    finish();
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = alert_box.create();
            dialog.show();
        } catch (Exception e) {
            // Tracking exception
        }
    }
}
