package com.skan_kontroll;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import Comman.Common;
import Comman.DrawerArrowDrawable;
import Comman.MyTextView_Regular;
import DB.DBClass;
import DB.sharedPrefrances;
import Fragment.Approval_Fragment;
import Tabs.Approve_TabContainer;
import Tabs.Calendar_TabContainer;
import Tabs.Leave_TabContainer;

public class Home_Screen extends FragmentActivity implements View.OnClickListener {

    private String TAB_1_TAG = "";
    private String TAB_2_TAG = "";
    private String TAB_3_TAG = "";
    private String TAB_4_TAG = "";
    private String TAB_5_TAG = "";
    private ImageView mdrawer_indicator;
    public static LinearLayout mLinearLayout_Plus;
    private DrawerArrowDrawable mdrawerArrowDrawable;
    private DrawerLayout mdrawer;
    private FragmentTabHost mTabHost;
    private MyTextView_Regular mMyTextView_Regular_selectedDate, mMyTextView_Regular_UserName;
    private LinearLayout mLinearLayout_PreviousDate, mLinearLayout_NextDate;
    public static LinearLayout mLinearLayout_DateLayout;
    private Calendar mCalendar;
    private SimpleDateFormat df;
    private SimpleDateFormat df_DB;
    private DatePickerDialog.OnDateSetListener date;
    public static String formattedDateForDB = "";
    private LinearLayout mLinearLayout_Logout;
    private DBClass SkanDB;
    private boolean backPressedToExitOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home__screen);
        try {
            // /////Initializing  DB
            SkanDB = DBClass.getDBAdapterInstance(Home_Screen.this);
            //Open DB once created
            SkanDB.openDataBase();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //// Initializing all static view in this function
        Init();
        setClicksListener();
        Common.getRaduis(Home_Screen.this);
        //// change top status bar color same as Action bar color
        changeStatusBarColor();
    }
    //// change top status bar color same as Action bar color
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = Home_Screen.this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(Home_Screen.this.getResources().getColor(R.color.Status_Bar_Color));
        }
    }
    @Override
    public void onUserInteraction() {
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentDate = new Date();
        String currentDateTimeString = df.format(currentDate);

        sharedPrefrances.saveActivityTimeStamp(Home_Screen.this, currentDateTimeString);
        Log.e("Touching",
                "Touching done......");
    }

    //// Initializing all static view in this function
    private void Init() {
        TAB_1_TAG = Home_Screen.this.getResources().getString(R.string.Approve);
        TAB_2_TAG = Home_Screen.this.getResources().getString(R.string.Calendar);
        TAB_3_TAG = Home_Screen.this.getResources().getString(R.string.Leave);
        TAB_4_TAG = Home_Screen.this.getResources().getString(R.string.Previous);
        TAB_5_TAG = Home_Screen.this.getResources().getString(R.string.Notifications);
        ///////////
        mdrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mdrawer_indicator = (ImageView) findViewById(R.id.drawer_indicator);
        final Resources resources = getResources();
        mdrawerArrowDrawable = new DrawerArrowDrawable(resources);
        mdrawerArrowDrawable.setStrokeColor(resources
                .getColor(R.color.white));
        mdrawer_indicator.setImageDrawable(mdrawerArrowDrawable);
        mdrawer_indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mdrawer.isDrawerVisible(Gravity.RIGHT)) {
                    mdrawer.closeDrawer(Gravity.RIGHT);
                } else {
                    mdrawer.openDrawer(Gravity.RIGHT);
                }
            }
        });
        mLinearLayout_Plus = (LinearLayout) findViewById(R.id.LinearLayout_Plus);
        ///////// Initializing Tab View
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
        mTabHost.getTabWidget().setDividerDrawable(null);
        ////////// Adding tabs in Fragment Tab Host one by one
        mTabHost.addTab(setIndicator(Home_Screen.this, mTabHost.newTabSpec(TAB_1_TAG),
                R.drawable.tab_indicator_gen, getResources().getString(R.string.Approve), R.drawable.thumb_hover), Approve_TabContainer.class, null);
        mTabHost.addTab(setIndicator(Home_Screen.this, mTabHost.newTabSpec(TAB_2_TAG),
                R.drawable.tab_indicator_gen, getResources().getString(R.string.Calendar), R.drawable.calendar), Calendar_TabContainer.class, null);
        mTabHost.addTab(setIndicator(Home_Screen.this, mTabHost.newTabSpec(TAB_3_TAG),
                R.drawable.tab_indicator_gen, getResources().getString(R.string.Leave), R.drawable.apply_leave), Leave_TabContainer.class, null);
//        mTabHost.addTab(setIndicator(Home_Screen.this, mTabHost.newTabSpec(TAB_4_TAG),
//                R.drawable.tab_indicator_gen, getResources().getString(R.string.Previous), R.drawable.task_history), Leave_TabContainer.class, null);
//        mTabHost.addTab(setIndicator(Home_Screen.this, mTabHost.newTabSpec(TAB_5_TAG),
//                R.drawable.tab_indicator_gen, getResources().getString(R.string.Notifications), R.drawable.notification), Leave_TabContainer.class, null);
        //////////Disabling tab for now
//        mTabHost.getTabWidget().getChildTabViewAt(1).setEnabled(false);
//        mTabHost.getTabWidget().getChildTabViewAt(2).setEnabled(false);
//        mTabHost.getTabWidget().getChildTabViewAt(3).setEnabled(false);
//        mTabHost.getTabWidget().getChildTabViewAt(4).setEnabled(false);
        //////////////Initializing Text view
        mMyTextView_Regular_selectedDate = (MyTextView_Regular) findViewById(R.id.MyTextView_selectedDate);
        setcurrentDate(mMyTextView_Regular_selectedDate);
        mMyTextView_Regular_UserName = (MyTextView_Regular) findViewById(R.id.MyTextView_UserName);
        String User_Name = sharedPrefrances.getUserDisplayName(Home_Screen.this);
        mMyTextView_Regular_UserName.setText(User_Name);
        //////////////////Initializing LinearLayout
        mLinearLayout_PreviousDate = (LinearLayout) findViewById(R.id.LinearLayout_PreviousDate);
        mLinearLayout_NextDate = (LinearLayout) findViewById(R.id.LinearLayout_NextDate);
        mLinearLayout_Logout = (LinearLayout) findViewById(R.id.LinearLayout_Logout);
        mLinearLayout_DateLayout = (LinearLayout) findViewById(R.id.LinearLayout_DateLayout);
    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
        mLinearLayout_PreviousDate.setOnClickListener(this);
        mLinearLayout_NextDate.setOnClickListener(this);
        mLinearLayout_Plus.setOnClickListener(this);
        mMyTextView_Regular_selectedDate.setOnClickListener(this);
        mLinearLayout_Logout.setOnClickListener(this);
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
                    View view = mTabHost.getTabWidget().getChildTabViewAt(i);

                    if (view != null) {
                        // get title text view
                        TextView textView = (TextView) view.findViewById(R.id.txt_tabtxt);
                        /// get Image view
                        ImageView imageView = (ImageView) view.findViewById(R.id.img_tabtxt);
                        String selected_Tab_Titel = textView.getText().toString();
                        if (selected_Tab_Titel.equalsIgnoreCase(tabId)) {
                            if (TAB_1_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
                                textView.setTextColor(Color.parseColor("#53aed1"));
                                imageView.setImageResource(R.drawable.thumb_hover);
                            }
                            if (TAB_2_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
                                textView.setTextColor(Color.parseColor("#53aed1"));
                                imageView.setImageResource(R.drawable.calendar_hover);
                            }
                            if (TAB_3_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
                                textView.setTextColor(Color.parseColor("#53aed1"));
                                imageView.setImageResource(R.drawable.apply_leave_hover);
                            }
//                            if (TAB_4_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
//                                textView.setTextColor(Color.parseColor("#9f9f9f"));
//                                imageView.setImageResource(R.drawable.task_history);
//                            }
//                            if (TAB_5_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
//                                textView.setTextColor(Color.parseColor("#9f9f9f"));
//                                imageView.setImageResource(R.drawable.notification);
//                            }
                        } else {
                            if (TAB_1_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
                                textView.setTextColor(Color.parseColor("#9f9f9f"));
                                imageView.setImageResource(R.drawable.thumb);
                            }
                            if (TAB_2_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
                                textView.setTextColor(Color.parseColor("#9f9f9f"));
                                imageView.setImageResource(R.drawable.calendar);
                            }
                            if (TAB_3_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
                                textView.setTextColor(Color.parseColor("#9f9f9f"));
                                imageView.setImageResource(R.drawable.apply_leave);
                            }
//                            if (TAB_4_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
//                                textView.setTextColor(Color.BLACK);
//                                imageView.setImageResource(R.drawable.task_history);
//                            }
//                            if (TAB_5_TAG.equalsIgnoreCase(selected_Tab_Titel)) {
//                                textView.setTextColor(Color.BLACK);
//                                imageView.setImageResource(R.drawable.notification);
//                            }

                        }
                    }
                }
            }
        });
    }

    ////////////////// Setting current date in textview
    private void setcurrentDate(TextView textView) {
        mCalendar = Calendar.getInstance();
        df = new SimpleDateFormat("dd/MM/yyyy");
        df_DB = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(mCalendar.getTime());
        formattedDateForDB = df_DB.format(mCalendar.getTime());
        textView.setText(formattedDate);

    }

    // dialog shown to confirm logout
    private void logoutDialog() {
        try {
            AlertDialog.Builder alert_box = new AlertDialog.Builder(Home_Screen.this);
            alert_box.setTitle(R.string.Dialog_Title);
            alert_box.setCancelable(false);
            alert_box.setMessage(R.string.Logout_msg);
            alert_box.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    ///////// user user selected yes then making changes in DB accordingly
                    ////////////// Deleting user data
                    sharedPrefrances.SaveFirstRun(Home_Screen.this, 0);
                    sharedPrefrances.saveUserID(Home_Screen.this, "");
                    sharedPrefrances.saveUserName(Home_Screen.this, "");
                    sharedPrefrances.saveUserEmail(Home_Screen.this, "");
                    sharedPrefrances.saveUserFirstname(Home_Screen.this, "");
                    sharedPrefrances.saveUserLastName(Home_Screen.this, "");
                    sharedPrefrances.saveUserDisplayName(Home_Screen.this, "");
                    sharedPrefrances.saveEnteredPassword(Home_Screen.this, "");
                    sharedPrefrances.saveEnteredUserName(Home_Screen.this, "");
                    sharedPrefrances.saveActivityTimeStamp(Home_Screen.this, "");
                    SkanDB.deleteApprovalDataInDB();
                    SkanDB.deleteLeaveDataInDB();
                    Intent intent = new Intent(Home_Screen.this, Login_Screen.class);
                    startActivity(intent);
                    finish();
                    dialog.dismiss();
                }
            });
            alert_box.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = alert_box.create();
            dialog.show();
        } catch (Exception e) {
            // Tracking exception
        }
    }

    @Override
    public void onBackPressed() {
//        boolean isPopFragment = false;
//        String currentTabTag = mTabHost.getCurrentTabTag();
//
//        if (currentTabTag.equals(TAB_1_TAG)) {
//            isPopFragment = ((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(TAB_1_TAG)).popFragment();
//        } else if (currentTabTag.equals(TAB_2_TAG)) {
//            isPopFragment = ((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(TAB_2_TAG)).popFragment();
//        } else if (currentTabTag.equals(TAB_3_TAG)) {
//            isPopFragment = ((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(TAB_3_TAG)).popFragment();
//        } else if (currentTabTag.equals(TAB_4_TAG)) {
//            isPopFragment = ((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(TAB_4_TAG)).popFragment();
//        } else if (currentTabTag.equals(TAB_5_TAG)) {
//            isPopFragment = ((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(TAB_5_TAG)).popFragment();
//        }
//        if (!isPopFragment) {
//            finish();
//        }
        if (backPressedToExitOnce) {
            this.finish();
        } else {
            this.backPressedToExitOnce = true;
            SnackbarManager.show(
                    Snackbar.with(Home_Screen.this)
                            .text(R.string.Back_msg));
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    mdrawer.closeDrawers();
                    backPressedToExitOnce = false;
                }
            }, 2000);
        }
    }

    private void showingDatePicker() {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, monthOfYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
    }

    /////////////// Setting tabs views
    private TabSpec setIndicator(Context ctx, TabSpec spec,
                                 int resid, String string, int genresIcon) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.tab_item, null);
        v.setBackgroundResource(resid);
        TextView tv = (TextView) v.findViewById(R.id.txt_tabtxt);
        ImageView img = (ImageView) v.findViewById(R.id.img_tabtxt);
        if (string.equalsIgnoreCase(getResources().getString(R.string.Approve))) {
            tv.setTextColor(Color.parseColor("#53aed1"));
        }
        tv.setText(string);
        img.setImageResource(genresIcon);
        return spec.setIndicator(v);
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

    }

    private void updateLabel() {
        /// Setting selected  date on Textview
        mMyTextView_Regular_selectedDate.setText(df.format(mCalendar.getTime()));
        formattedDateForDB = df_DB.format(mCalendar.getTime());
        Approval_Fragment.approval_fragment.change_Approval_List(formattedDateForDB);
    }

    @Override
    public void onClick(View view) {
        if (view == mLinearLayout_PreviousDate) {
            mCalendar.add(Calendar.DATE, -1);
            String formattedDate = df.format(mCalendar.getTime());
            formattedDateForDB = df_DB.format(mCalendar.getTime());
            /// Setting previous date on Textview
            mMyTextView_Regular_selectedDate.setText(formattedDate);
            Approval_Fragment.approval_fragment.change_Approval_List(formattedDateForDB);
        } else if (view == mLinearLayout_NextDate) {
            mCalendar.add(Calendar.DATE, 1);
            String formattedDate = df.format(mCalendar.getTime());
            formattedDateForDB = df_DB.format(mCalendar.getTime());
            /// Setting previous date on Textview
            mMyTextView_Regular_selectedDate.setText(formattedDate);
            Approval_Fragment.approval_fragment.change_Approval_List(formattedDateForDB);
        } else if (view == mMyTextView_Regular_selectedDate) {
            showingDatePicker();
            new DatePickerDialog(Home_Screen.this, date, mCalendar
                    .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                    mCalendar.get(Calendar.DAY_OF_MONTH)).show();

        } else if (view == mLinearLayout_Logout) {
            mdrawer.closeDrawers();
            logoutDialog();
        } else if (view == mLinearLayout_Plus) {
            if (Common.Fragment_No == 3) {
                Intent intent = new Intent(Home_Screen.this, Add_leaves_Screen.class);
                startActivity(intent);
            }
        }
    }

}