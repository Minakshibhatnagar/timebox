package com.skan_kontroll;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import Comman.MyTextView_Regular;
import Comman.ProgressHUD;
import Comman.Webservices;
import DB.sharedPrefrances;
import Model.Approval_Model;


public class Current_Address_Map_Screen extends FragmentActivity implements View.OnClickListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private LinearLayout mLinearLayout_Back, mLinearLayout_route;
    private MyTextView_Regular mMyTextView_Regular_Address, mMyTextView_Regular_UserAddress, mMyTextView_Regular_Street, mMyTextView_Regular_PostalCode, mMyTextView_Regular_City;
    private Approval_Model mApproval_model;
    private String googleAPI_Response = "";
    private ProgressHUD mProgressHUD;
    private String finalAddress = "";
    private String MarkerAddress = "";
    final private int Request_Location = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current__address__map);
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            String beforeAsked = sharedPrefrances.getMapPermissions(Current_Address_Map_Screen.this);
            if (beforeAsked == null || beforeAsked.equalsIgnoreCase("0")) {
                // only for gingerbread and newer versions
                ActivityCompat.requestPermissions(Current_Address_Map_Screen.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, Request_Location);
            }else {
                setUpMapIfNeeded();
                init();
                setClicksListener();

                ////Checking if internet is available or not
                if (Webservices.isInternetOn(Current_Address_Map_Screen.this)) {
                    /////////Getting lat long using reverse GeoCoding API
                    new GetAddressLatLong().execute();
                }
            }

        } else {
            setUpMapIfNeeded();
            //// Initializing all static view in this function
            init();
            ///// Setting all views click Listener In this function
            setClicksListener();
            //// change top status bar color same as Action bar color
            changeStatusBarColor();

            ////Checking if internet is available or not
            if (Webservices.isInternetOn(Current_Address_Map_Screen.this)) {
                /////////Getting lat long using reverse GeoCoding API
                new GetAddressLatLong().execute();
            }
        }
    }
    //// change top status bar color same as Action bar color
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = Current_Address_Map_Screen.this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(Current_Address_Map_Screen.this.getResources().getColor(R.color.Status_Bar_Color));
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Request_Location:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sharedPrefrances.saveMapPermissions(Current_Address_Map_Screen.this, "1");
                    setUpMapIfNeeded();
                    init();
                    setClicksListener();

                    ////Checking if internet is available or not
                    if (Webservices.isInternetOn(Current_Address_Map_Screen.this)) {
                        /////////Getting lat long using reverse GeoCoding API
                        new GetAddressLatLong().execute();
                    }
                } else {
                    sharedPrefrances.saveMapPermissions(Current_Address_Map_Screen.this, "0");
                    SnackbarManager.show(
                            Snackbar.with(Current_Address_Map_Screen.this)
                                    .text(R.string.Denied_Permission_error));
                }

                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap(0.0, 0.0)} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap(0.0, 0.0);
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap(double lat, double lng) {

//        Marker marker = mMap.addMarker(new MarkerOptions()
//                .position(new LatLng(30.7046, 76.7179))
//                .title("Title")
//                .snippet("Snippet")
//                );
//
//        marker.showInfoWindow();
        if (lat != 0.0 && lng != 0.0) {
            String address = mApproval_model.getAddress();
            mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(address)
                    .snippet(MarkerAddress));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14.0f));
        }
    }

    //// Initializing all static view in this function
    private void init() {
        // 1. get passed intent
        Intent intent = getIntent();
        // 2. get Approval_Model object from intent which is sent from previous activity
        mApproval_model = (Approval_Model) intent.getSerializableExtra("Address");
        ////////////////
        mLinearLayout_Back = (LinearLayout) findViewById(R.id.LinearLayout_Back);
        mLinearLayout_route = (LinearLayout) findViewById(R.id.LinearLayout_route);
        ////////////
        mMyTextView_Regular_Address = (MyTextView_Regular) findViewById(R.id.MyTextView_Address);
        mMyTextView_Regular_UserAddress = (MyTextView_Regular) findViewById(R.id.MyTextView_UserAddress);
        mMyTextView_Regular_Street = (MyTextView_Regular) findViewById(R.id.MyTextView_Street);
        mMyTextView_Regular_PostalCode = (MyTextView_Regular) findViewById(R.id.MyTextView_PostalCode);
        mMyTextView_Regular_City = (MyTextView_Regular) findViewById(R.id.MyTextView_City);
        /////////////// Setting data in Textviews
        String address = mApproval_model.getAddress();
        String street = mApproval_model.getStreet();
        String visitCityPostalCode = mApproval_model.getVisitCityPostalCode();
        String city = mApproval_model.getCity();
        ///////////
        mMyTextView_Regular_Address.setText(address);
        mMyTextView_Regular_UserAddress.setText(address);
        mMyTextView_Regular_Street.setText(street);
        mMyTextView_Regular_PostalCode.setText(visitCityPostalCode);
        mMyTextView_Regular_City.setText(city);
    }

    ///// Setting all views click Listener In this function
    private void setClicksListener() {
        mLinearLayout_Back.setOnClickListener(this);
//        mLinearLayout_route.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mLinearLayout_Back) {
            finish();
        }

    }

    /////////Getting lat long using reverse GeoCoding API
    private class GetAddressLatLong extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // showing progress bar
            mProgressHUD = ProgressHUD.show(Current_Address_Map_Screen.this, true,
                    false, this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String address = mApproval_model.getAddress();
                String street = mApproval_model.getStreet();
                String visitCityPostalCode = mApproval_model.getVisitCityPostalCode();
                String city = mApproval_model.getCity();
                MarkerAddress = address + "\n" + street + "\n" + visitCityPostalCode + "\n" + city;
                finalAddress = address + " " + street + " " + visitCityPostalCode + " " + city;

                StringBuilder stringBuilder = new StringBuilder();
                try {
                    finalAddress = finalAddress.replaceAll(" ", "%20");
                    //// hitting google API to get user current address on basis on user lat and long
                    String url = "http://maps.googleapis.com/maps/api/geocode/json?address="
                            + finalAddress + "&sensor=false";
                    HttpPost httppost = new HttpPost(url);
                    HttpClient client = new DefaultHttpClient();
                    HttpResponse response;
                    stringBuilder = new StringBuilder();


                    response = client.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    InputStream stream = entity.getContent();
                    int b;
                    while ((b = stream.read()) != -1) {
                        stringBuilder.append((char) b);
                    }
                } catch (ClientProtocolException e) {
                } catch (IOException e) {
                }
                googleAPI_Response = "";
                googleAPI_Response = stringBuilder.toString();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {
                if (googleAPI_Response.length() > 0) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject = new JSONObject(googleAPI_Response);

                        double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                                .getJSONObject("geometry").getJSONObject("location")
                                .getDouble("lng");

                        double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                                .getJSONObject("geometry").getJSONObject("location")
                                .getDouble("lat");
                        setUpMap(lat, lng);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        setUpMap(0.0, 0.0);
                        mProgressHUD.dismiss();
                    }
                } else {
                    setUpMap(0.0, 0.0);
                }
                mProgressHUD.dismiss();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            // TODO Auto-generated method stub
            this.cancel(true);

            mProgressHUD.dismiss();
        }
    }
    @Override
    public void onUserInteraction() {
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentDate = new Date();
        String currentDateTimeString = df.format(currentDate);

        sharedPrefrances.saveActivityTimeStamp(Current_Address_Map_Screen.this, currentDateTimeString);
        Log.e("Touching",
                "Touching done......");
    }
}
