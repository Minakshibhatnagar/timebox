package Model;

/**
 * Created by Nitin Sood on 23-04-2016.
 */
public class Leave_Model {

    private String Id;
    private String LeaveStartDate;
    private String LeaveEndDate;
    private String LeaveType;
    private String WeekNumber;
    private String ApprovalStatusId;
    private String LeaveId;


    public Leave_Model(String Id, String LeaveStartDate, String LeaveEndDate
            , String LeaveType, String WeekNumber, String ApprovalStatusId
            , String LeaveId) {//settings values in getters and setters
        super();

        this.Id = Id;
        this.LeaveStartDate = LeaveStartDate;
        this.LeaveEndDate = LeaveEndDate;
        this.LeaveType = LeaveType;
        this.WeekNumber = WeekNumber;
        this.ApprovalStatusId = ApprovalStatusId;
        this.LeaveId = LeaveId;

    }

    public String getLeaveId() {
        return LeaveId;
    }

    public void setLeaveId(String leaveId) {
        LeaveId = leaveId;
    }

    public String getApprovalStatusId() {
        return ApprovalStatusId;
    }

    public void setApprovalStatusId(String approvalStatusId) {
        ApprovalStatusId = approvalStatusId;
    }

    public String getLeaveType() {
        return LeaveType;
    }

    public void setLeaveType(String leaveType) {
        LeaveType = leaveType;
    }

    public String getLeaveEndDate() {
        return LeaveEndDate;
    }

    public void setLeaveEndDate(String leaveEndDate) {
        LeaveEndDate = leaveEndDate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getLeaveStartDate() {
        return LeaveStartDate;
    }

    public void setLeaveStartDate(String leaveStartDate) {
        LeaveStartDate = leaveStartDate;
    }

    public String getWeekNumber() {
        return WeekNumber;
    }

    public void setWeekNumber(String weekNumber) {
        WeekNumber = weekNumber;
    }
}
