package Model;

/**
 * Created by Nitin Sood on 01-04-2016.
 */
import java.io.Serializable;
public class Approval_Model implements Serializable{

    private String UserIdd;
    private String StartDate;
    private String EndDate;
    private String CustomerName;
    private String Article_Details;
    private String Logged_Hours;
    private String Planned_Hours;
    private String Has_Comment;
    private String Comment;
    private String Task_bg_color;
    private String Task_text_color;
    private String IssueId;
    private String WorkDate;
    private String IssueWorkReportId;
    private String RequestId;
    private String ApprovalStatusId;
    private String IssueCommentId;
    private String Code;
    private String TelephoneNumber1;
    private String TelephoneNumber2;
    private String Address;
    private String Street;
    private String VisitCityPostalCode;
    private String City;
    private String CustomerComment;
    private String Flag;
    private String SchedulerComment;

    public Approval_Model(String UserIdd, String StartDate, String EndDate
            , String CustomerName, String Article_Details, String Logged_Hours
            , String Planned_Hours, String Has_Comment, String Comment
            , String Task_bg_color, String Task_text_color, String IssueId
            , String WorkDate, String IssueWorkReportId, String RequestId
            , String ApprovalStatusId, String IssueCommentId, String Code
            , String TelephoneNumber1, String TelephoneNumber2, String Address
            , String Street, String VisitCityPostalCode, String City
            , String CustomerComment, String Flag, String SchedulerComment) {//settings values in getters and setters
        super();

        this.UserIdd = UserIdd;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.CustomerName = CustomerName;
        this.Article_Details = Article_Details;
        this.Logged_Hours = Logged_Hours;
        this.Planned_Hours = Planned_Hours;
        this.Has_Comment = Has_Comment;
        this.Comment = Comment;
        this.Task_bg_color = Task_bg_color;
        this.Task_text_color = Task_text_color;
        this.IssueId = IssueId;
        this.WorkDate = WorkDate;
        this.IssueWorkReportId = IssueWorkReportId;
        this.RequestId = RequestId;
        this.ApprovalStatusId = ApprovalStatusId;
        this.IssueCommentId = IssueCommentId;
        this.Code = Code;
        this.TelephoneNumber1 = TelephoneNumber1;
        this.TelephoneNumber2 = TelephoneNumber2;
        this.Address = Address;
        this.Street = Street;
        this.VisitCityPostalCode = VisitCityPostalCode;
        this.City = City;
        this.CustomerComment = CustomerComment;
        this.Flag = Flag;
        this.SchedulerComment = SchedulerComment;
    }


    public String getUserIdd() {
        return UserIdd;
    }

    public void setUserIdd(String userIdd) {
        UserIdd = userIdd;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getArticle_Details() {
        return Article_Details;
    }

    public void setArticle_Details(String article_Details) {
        Article_Details = article_Details;
    }

    public String getLogged_Hours() {
        return Logged_Hours;
    }

    public void setLogged_Hours(String logged_Hours) {
        Logged_Hours = logged_Hours;
    }

    public String getPlanned_Hours() {
        return Planned_Hours;
    }

    public void setPlanned_Hours(String planned_Hours) {
        Planned_Hours = planned_Hours;
    }

    public String getHas_Comment() {
        return Has_Comment;
    }

    public void setHas_Comment(String has_Comment) {
        Has_Comment = has_Comment;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getTask_bg_color() {
        return Task_bg_color;
    }

    public void setTask_bg_color(String task_bg_color) {
        Task_bg_color = task_bg_color;
    }

    public String getTask_text_color() {
        return Task_text_color;
    }

    public void setTask_text_color(String task_text_color) {
        Task_text_color = task_text_color;
    }

    public String getIssueId() {
        return IssueId;
    }

    public void setIssueId(String issueId) {
        IssueId = issueId;
    }

    public String getWorkDate() {
        return WorkDate;
    }

    public void setWorkDate(String workDate) {
        WorkDate = workDate;
    }

    public String getIssueWorkReportId() {
        return IssueWorkReportId;
    }

    public void setIssueWorkReportId(String issueWorkReportId) {
        IssueWorkReportId = issueWorkReportId;
    }

    public String getRequestId() {
        return RequestId;
    }

    public void setRequestId(String requestId) {
        RequestId = requestId;
    }

    public String getApprovalStatusId() {
        return ApprovalStatusId;
    }

    public void setApprovalStatusId(String approvalStatusId) {
        ApprovalStatusId = approvalStatusId;
    }

    public String getIssueCommentId() {
        return IssueCommentId;
    }

    public void setIssueCommentId(String issueCommentId) {
        IssueCommentId = issueCommentId;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getTelephoneNumber1() {
        return TelephoneNumber1;
    }

    public void setTelephoneNumber1(String telephoneNumber1) {
        TelephoneNumber1 = telephoneNumber1;
    }

    public String getTelephoneNumber2() {
        return TelephoneNumber2;
    }

    public void setTelephoneNumber2(String telephoneNumber2) {
        TelephoneNumber2 = telephoneNumber2;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getVisitCityPostalCode() {
        return VisitCityPostalCode;
    }

    public void setVisitCityPostalCode(String visitCityPostalCode) {
        VisitCityPostalCode = visitCityPostalCode;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCustomerComment() {
        return CustomerComment;
    }

    public void setCustomerComment(String customerComment) {
        CustomerComment = customerComment;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getSchedulerComment() {
        return SchedulerComment;
    }

    public void setSchedulerComment(String schedulerComment) {
        SchedulerComment = schedulerComment;
    }


}
