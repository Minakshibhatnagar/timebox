package Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.skan_kontroll.Approval_ChangeHrs_Screen;
import com.skan_kontroll.Approval_Options_Screen;
import com.skan_kontroll.Current_Address_Map_Screen;
import com.skan_kontroll.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Comman.Common;
import Comman.MyTextView_Regular;
import Fragment.Approval_Fragment;
import Model.Approval_Model;

/**
 * Created by nitin_sood on 20-11-2015.
 */
public class Approvel_Adapter extends BaseAdapter {

    private Context context;
    private List<Approval_Model> rowItem;
    private boolean flagForError = true;
    private Map<Integer, View> views = new HashMap<Integer, View>();

    public Approvel_Adapter(Context context, List<Approval_Model> rowItem) {
        System.out.println("rowItem :" + rowItem.size());
        this.context = context;
        this.rowItem = rowItem;
        // /////Creating DB

    }

    public void clearAdapter() {
        if (rowItem != null) {
            rowItem.clear();
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (views.containsKey(position)) {
            return views.get(position);
        }
        View view = null;
        ////////////////// Getting data position wise
        final Approval_Model row_pos = rowItem.get(position);
        String cust_name = row_pos.getCustomerName();
        String Article_Details = row_pos.getArticle_Details();
        String PlannedHrs = row_pos.getPlanned_Hours();
        String LoggedHRS = row_pos.getLogged_Hours();
        if (LoggedHRS.length() > 0) {
            String before = Common.getMeNthParamInString(LoggedHRS, ".", 1);
            String after = Common.getMeNthParamInString(LoggedHRS, ".", 2);
            if (after.length() == 1) {
                after = after + "0";
                LoggedHRS = before + "." + after;
            }
        }
        String startdate = row_pos.getStartDate();
        String EndDate = row_pos.getEndDate();
        String custComment = row_pos.getCustomerComment();
        String SchedulerComment = row_pos.getSchedulerComment();
        String task_Bg_Color = row_pos.getTask_bg_color();
        if (task_Bg_Color.equalsIgnoreCase("")) {
            task_Bg_Color = "#ffffff";
        }
        String task_text_bg = row_pos.getTask_text_color();
        if (task_text_bg.equalsIgnoreCase("")) {
            task_text_bg = "#ffffff";
        }
        String ApprovalStatusId = row_pos.getApprovalStatusId();
        String topStartTime = convertDate(startdate);
        String topEndTime = convertDate(EndDate);
        /////////// Checking if date is past of future and then show list accordingly
        String listFlag = Common.checkIfEndDateIsPastOrFuture(EndDate);
        if (listFlag.equalsIgnoreCase("Future")) {
            ///////////////////////////
//            if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.approvel_list_view_future,
                    null);
//            }
//            LinearLayout LinearLayout_HrsMainLay = (LinearLayout) convertView
//                    .findViewById(R.id.LinearLayout_HrsMainLay);
//            LinearLayout_HrsMainLay.setVisibility(View.GONE);
//            ///////////////////
//            LinearLayout LinearLayout_WeightLay = (LinearLayout) convertView
//                    .findViewById(R.id.LinearLayout_WeightLay);
//            LinearLayout_WeightLay.setWeightSum(2);
//            ///////////////////////////
//            LinearLayout LinearLayout_ArticalLay = (LinearLayout) convertView
//                    .findViewById(R.id.LinearLayout_ArticalLay);
//
//
//
////            LinearLayout_ArticalLay.
//
//            //////////////////
//            LinearLayout LinearLayout_CustLay = (LinearLayout) convertView
//                    .findViewById(R.id.LinearLayout_CustLay);
//            ///////////////

            Approval_Fragment.menuFlag = 22;
            LinearLayout LinearLayout_DateLay = (LinearLayout) view
                    .findViewById(R.id.LinearLayout_DateLay);
            RoundRectShape rect = new RoundRectShape(
                    new float[]{30, 30, 30, 30, 30, 30, 30, 30},
                    null,
                    null);
            int raduis = Common.getRaduis(context);
            LinearLayout_DateLay.setBackgroundResource(R.drawable.round_top_layout);
            GradientDrawable gd = (GradientDrawable) LinearLayout_DateLay.getBackground().getCurrent();
            gd.setColor(Color.parseColor(task_Bg_Color));
            gd.setCornerRadii(new float[]{raduis, raduis, raduis, raduis, 0, 0, 0, 0});
            gd.setStroke(1, Color.parseColor(task_Bg_Color), 5, 6);
            ////////////////////////////////////////////
            /////////////////////////////////
            ImageView ImageView_Approve = (ImageView) view
                    .findViewById(R.id.ImageView_Approve);
            ImageView_Approve.setImageResource(R.drawable.future);
            ImageView_Approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Approval_Fragment.mListView.smoothOpenMenu(position);
                }
            });
            ///////////////
            ImageView ImageView_Map = (ImageView) view
                    .findViewById(R.id.ImageView_Map);
            ImageView_Map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String address = row_pos.getAddress().trim();
                    String street = row_pos.getStreet().trim();
                    if (address.length() > 0 && street.length() > 0 && address != null && street != null) {
                        Intent i = new Intent(context, Current_Address_Map_Screen.class);
                        i.putExtra("Address", row_pos);
                        context.startActivity(i);
                    }
                }
            });
            ////////////////////
            LinearLayout LinearLayout_Info = (LinearLayout) view
                    .findViewById(R.id.LinearLayout_Info);
            LinearLayout_Info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, Approval_Options_Screen.class);
                    i.putExtra("ModelClass", row_pos);
                    context.startActivity(i);
                }
            });
            //////////// Top Time
            MyTextView_Regular myTextView_Regular_Time = (MyTextView_Regular) view
                    .findViewById(R.id.MyTextView_Time);
            myTextView_Regular_Time.setTextColor(Color.parseColor(task_text_bg));
            myTextView_Regular_Time.setText(topStartTime + " - " + topEndTime);
            myTextView_Regular_Time.setTypeface(Common.getfontStyle(context, Common.Fontstyle.FONT_BOLDITALIC));
            ////////////////////////////
            MyTextView_Regular myTextView_Regular_CustName = (MyTextView_Regular) view
                    .findViewById(R.id.MyTextView_CustName);
            myTextView_Regular_CustName.setText(cust_name);
            ///////////////
            MyTextView_Regular myTextView_Regular_Article_Details = (MyTextView_Regular) view
                    .findViewById(R.id.MyTextView_Article_Details);
            myTextView_Regular_Article_Details.setText(Article_Details);
            myTextView_Regular_Article_Details.setTypeface(Common.getfontStyle(context, Common.Fontstyle.FONT_REGULAR));
            ////////////
            if (custComment.length() == 0 && SchedulerComment.length() == 0) {
                LinearLayout_Info.setVisibility(View.INVISIBLE);
            } else {
                LinearLayout_Info.setVisibility(View.VISIBLE);
            }
            flagForError = false;
        } else {
            if (ApprovalStatusId.equalsIgnoreCase("1") || ApprovalStatusId.equalsIgnoreCase("4")) {
                ///////////////////////////

//                if (convertView == null) {
                LayoutInflater mInflater = (LayoutInflater) context
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                view = mInflater.inflate(R.layout.approvel_list_view_past,
                        null);
//                }
//                LinearLayout LinearLayout_HrsMainLay = (LinearLayout) convertView
//                        .findViewById(R.id.LinearLayout_HrsMainLay);
//                LinearLayout_HrsMainLay.setVisibility(View.VISIBLE);
//                LinearLayout LinearLayout_WeightLay = (LinearLayout) convertView
//                        .findViewById(R.id.LinearLayout_WeightLay);
//                LinearLayout_WeightLay.setWeightSum(3);


                Approval_Fragment.menuFlag = 44;
                LinearLayout LinearLayout_DateLay = (LinearLayout) view
                        .findViewById(R.id.LinearLayout_DateLay);
                RoundRectShape rect = new RoundRectShape(
                        new float[]{30, 30, 30, 30, 30, 30, 30, 30},
                        null,
                        null);
                int raduis = Common.getRaduis(context);
                LinearLayout_DateLay.setBackgroundResource(R.drawable.round_top_layout);
                GradientDrawable gd = (GradientDrawable) LinearLayout_DateLay.getBackground().getCurrent();
                gd.setColor(Color.parseColor(task_Bg_Color));
                gd.setCornerRadii(new float[]{raduis, raduis, raduis, raduis, 0, 0, 0, 0});
                gd.setStroke(1, Color.parseColor(task_Bg_Color), 5, 6);
                ////////////////////////////////////////////
//        text.setText(rowItem.get(position).loadLabel(context.getPackageManager()));
                /////////////////////////////////
                ImageView ImageView_Approve = (ImageView) view
                        .findViewById(R.id.ImageView_Approve);
                if (ApprovalStatusId.equalsIgnoreCase("1")) {
                    ImageView_Approve.setImageResource(R.drawable.pending);
                } else {
                    ImageView_Approve.setImageResource(R.drawable.rejected);
                }
                ImageView_Approve.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Approval_Fragment.mListView.smoothOpenMenu(position);
                    }
                });
                ///////////////
                ImageView ImageView_Map = (ImageView) view
                        .findViewById(R.id.ImageView_Map);
                ImageView_Map.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String address = row_pos.getAddress().trim();
                        String street = row_pos.getStreet().trim();
                        if (address.length() > 0 && street.length() > 0 && address != null && street != null) {
                            Intent i = new Intent(context, Current_Address_Map_Screen.class);
                            i.putExtra("Address", row_pos);
                            context.startActivity(i);
                        }
                    }
                });
                ////////////////////
                LinearLayout LinearLayout_Info = (LinearLayout) view
                        .findViewById(R.id.LinearLayout_Info);
                LinearLayout_Info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, Approval_Options_Screen.class);
                        i.putExtra("ModelClass", row_pos);
                        context.startActivity(i);
                    }
                });

                /////////////////////////////
                LinearLayout LinearLayout_PlannedHS = (LinearLayout) view
                        .findViewById(R.id.LinearLayout_PlannedHS);

                //////////// Top Time

                MyTextView_Regular myTextView_Regular_Time = (MyTextView_Regular) view
                        .findViewById(R.id.MyTextView_Time);
                myTextView_Regular_Time.setTextColor(Color.parseColor(task_text_bg));
                myTextView_Regular_Time.setText(topStartTime + " - " + topEndTime);
                myTextView_Regular_Time.setTypeface(Common.getfontStyle(context, Common.Fontstyle.FONT_BOLDITALIC));
                ////////////////////////////
                MyTextView_Regular myTextView_Regular_CustName = (MyTextView_Regular) view
                        .findViewById(R.id.MyTextView_CustName);
                myTextView_Regular_CustName.setText(cust_name);
                ///////////////
                MyTextView_Regular myTextView_Regular_Article_Details = (MyTextView_Regular) view
                        .findViewById(R.id.MyTextView_Article_Details);
                myTextView_Regular_Article_Details.setText(Article_Details);
                myTextView_Regular_Article_Details.setTypeface(Common.getfontStyle(context, Common.Fontstyle.FONT_BOLDITALIC));
                ///////////////////
                MyTextView_Regular myTextView_Regular_PlannedHrs = (MyTextView_Regular) view
                        .findViewById(R.id.MyTextView_PlannedHrs);
                if (myTextView_Regular_PlannedHrs != null) {
                    myTextView_Regular_PlannedHrs.setText(PlannedHrs);
                    myTextView_Regular_PlannedHrs.setTypeface(Common.getfontStyle(context, Common.Fontstyle.FONT_BOLDITALIC));
                }
                ///////////////
                MyTextView_Regular myTextView_Regular_LoggedHRS = (MyTextView_Regular) view
                        .findViewById(R.id.MyTextView_LoggedHRS);
                if (myTextView_Regular_LoggedHRS != null) {
                    myTextView_Regular_LoggedHRS.setText(LoggedHRS);
                    myTextView_Regular_LoggedHRS.setTypeface(Common.getfontStyle(context, Common.Fontstyle.FONT_BOLDITALIC));
                    myTextView_Regular_LoggedHRS.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(context, Approval_ChangeHrs_Screen.class);
                            i.putExtra("ModelClass", row_pos);
                            context.startActivity(i);
                        }
                    });
                }

                ////////////
                if (custComment.length() == 0 && SchedulerComment.length() == 0) {
                    LinearLayout_Info.setVisibility(View.INVISIBLE);
                } else {
                    LinearLayout_Info.setVisibility(View.VISIBLE);
                }
                flagForError = false;
            }
        }
        views.put(position, view);
        return view;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return rowItem.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItem.indexOf(getItem(position));
    }

    private String convertDate(String datestr) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat("HH:mm");
        Date date = null;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(datestr);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);
            result = output;
            //Displaying the date
            System.out.println(output);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }


}
