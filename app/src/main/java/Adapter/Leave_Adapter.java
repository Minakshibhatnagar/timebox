package Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.skan_kontroll.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Comman.MyTextView_Regular;
import Fragment.Leave_Fragment;
import Model.Leave_Model;

/**
 * Created by Nitin Sood on 23-04-2016.
 */
public class Leave_Adapter extends BaseAdapter {

    private Context context;
    private List<Leave_Model> rowItem;
    private boolean flagForError = true;
    private Map<Integer, View> views = new HashMap<Integer, View>();
    private ArrayList<String> daysArray = new ArrayList<String>();

    public Leave_Adapter(Context context, List<Leave_Model> rowItem) {
        System.out.println("rowItem :" + rowItem.size());
        this.context = context;
        this.rowItem = rowItem;
        // /////Creating DB

    }

    public void clearAdapter() {
        if (rowItem != null) {
            rowItem.clear();
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (views.containsKey(position)) {
            return views.get(position);
        }
        View view = null;
        ////////////////// Getting data position wise
        final Leave_Model row_pos = rowItem.get(position);
        String LeaveStartDate = row_pos.getLeaveStartDate();
        String LeaveEndDate = row_pos.getLeaveEndDate();
        String LeaveType = row_pos.getLeaveType();
        String WeekNumber = row_pos.getWeekNumber();
        String ApprovalStatusId = row_pos.getApprovalStatusId();
        String LeaveId = row_pos.getLeaveId();
        if (LeaveId.equalsIgnoreCase("3")) {
            LeaveType = context.getResources().getString(R.string.sick_leave);
        } else if (LeaveId.equalsIgnoreCase("4")) {
            LeaveType = context.getResources().getString(R.string.sick_child);
        } else if (LeaveId.equalsIgnoreCase("28")) {
            LeaveType = context.getResources().getString(R.string.vacation);
        }
        LeaveStartDate = convertDate(LeaveStartDate);
        LeaveEndDate = convertDate(LeaveEndDate);

        if (!ApprovalStatusId.equalsIgnoreCase("3")) {
            Leave_Fragment.menuFlag = 44;
        } else {
            Leave_Fragment.menuFlag = 22;
        }

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        view = mInflater.inflate(R.layout.leave_layout,
                null);
        ////////////// Initializing all the view
        MyTextView_Regular MyTextView_Regular_WeekNo = (MyTextView_Regular) view
                .findViewById(R.id.MyTextView_Regular_WeekNo);
        MyTextView_Regular MyTextView_Regular_LeaveType = (MyTextView_Regular) view
                .findViewById(R.id.MyTextView_Regular_LeaveType);

        MyTextView_Regular MyTextView_Regular_Date = (MyTextView_Regular) view
                .findViewById(R.id.MyTextView_Regular_Date);
        View View_Mon = (View) view
                .findViewById(R.id.View_Mon);
        View View_Tues = (View) view
                .findViewById(R.id.View_Tues);
        View View_Wed = (View) view
                .findViewById(R.id.View_Wed);
        View View_Thru = (View) view
                .findViewById(R.id.View_Thru);
        View View_Fri = (View) view
                .findViewById(R.id.View_Fri);
        View View_Sat = (View) view
                .findViewById(R.id.View_Sat);
        View View_Sun = (View) view
                .findViewById(R.id.View_Sun);
        daysArray.clear();
        daysArray = getAllThedays(LeaveStartDate, LeaveEndDate);
        if (daysArray.size() > 0) {
            for (int i = 0; i < daysArray.size(); i++) {
                String day_name = daysArray.get(i);
                if (day_name.equalsIgnoreCase(context.getResources().getString(R.string.Monday))) {
                    if (ApprovalStatusId.equalsIgnoreCase("3")) {
                        View_Mon.setBackgroundResource(R.drawable.round_leave_green);
                    } else {
                        View_Mon.setBackgroundResource(R.drawable.round_leave_yellow);
                    }

                } else if (day_name.equalsIgnoreCase(context.getResources().getString(R.string.Tuesday))) {
                    if (ApprovalStatusId.equalsIgnoreCase("3")) {
                        View_Tues.setBackgroundResource(R.drawable.round_leave_green);
                    } else {
                        View_Tues.setBackgroundResource(R.drawable.round_leave_yellow);
                    }
                } else if (day_name.equalsIgnoreCase(context.getResources().getString(R.string.Wednesday))) {
                    if (ApprovalStatusId.equalsIgnoreCase("3")) {
                        View_Wed.setBackgroundResource(R.drawable.round_leave_green);
                    } else {
                        View_Wed.setBackgroundResource(R.drawable.round_leave_yellow);
                    }
                } else if (day_name.equalsIgnoreCase(context.getResources().getString(R.string.Thursday))) {
                    if (ApprovalStatusId.equalsIgnoreCase("3")) {
                        View_Thru.setBackgroundResource(R.drawable.round_leave_green);
                    } else {
                        View_Thru.setBackgroundResource(R.drawable.round_leave_yellow);
                    }
                } else if (day_name.equalsIgnoreCase(context.getResources().getString(R.string.Friday))) {
                    if (ApprovalStatusId.equalsIgnoreCase("3")) {
                        View_Fri.setBackgroundResource(R.drawable.round_leave_green);
                    } else {
                        View_Fri.setBackgroundResource(R.drawable.round_leave_yellow);
                    }
                } else if (day_name.equalsIgnoreCase(context.getResources().getString(R.string.Saturday))) {
                    if (ApprovalStatusId.equalsIgnoreCase("3")) {
                        View_Sat.setBackgroundResource(R.drawable.round_leave_green);
                    } else {
                        View_Sat.setBackgroundResource(R.drawable.round_leave_yellow);
                    }
                } else if (day_name.equalsIgnoreCase(context.getResources().getString(R.string.Sunday))) {
                    if (ApprovalStatusId.equalsIgnoreCase("3")) {
                        View_Sun.setBackgroundResource(R.drawable.round_leave_green);
                    } else {
                        View_Sun.setBackgroundResource(R.drawable.round_leave_yellow);
                    }
                }
            }
        }

        /////////////// Setting data /////
        MyTextView_Regular_WeekNo.setText(WeekNumber);
        MyTextView_Regular_LeaveType.setText(LeaveType);
        MyTextView_Regular_Date.setText(LeaveStartDate + " - " + LeaveEndDate);
        ////////// Adding view to HashMap so that it will not add previous view again
        views.put(position, view);
        return view;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return rowItem.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItem.indexOf(getItem(position));
    }

    private String convertDate(String datestr) {
        String result = "";
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = null;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(datestr);
            //Changing the format of date and storing it in String
            output = outputformat.format(date);
            result = output;
            //Displaying the date
            System.out.println(output);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    private ArrayList<String> getAllThedays(String startDate, String endDate) {
        DateFormat outputformat = new SimpleDateFormat("EEEE");
        ArrayList<String> daysArray = new ArrayList<String>();
        DateFormat formatter;
        try {
            formatter = new SimpleDateFormat("dd.MM.yyyy");
            Date start = (Date) formatter.parse(startDate);
            Date end = (Date) formatter.parse(endDate);
            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
            long endTime = end.getTime(); // create your endtime here, possibly using Calendar or Date
            long curTime = start.getTime();
            while (curTime <= endTime) {
                Date Compare_date = new Date(curTime);
                String day_name = outputformat.format(Compare_date);
                daysArray.add(day_name);
                curTime += interval;
            }

        } catch (Exception e) {

        }
        return daysArray;
    }
}
